[![Website Build Status](https://gitlab.com/coviz-org/pelican/badges/master/pipeline.svg)](https://gitlab.com/coviz-org/pelican/-/commits/master)

## Repo Archived

**Update 2023-10 This project has been retired** because the upstream data source on COVID-19 infections has [stopped being updated](https://github.com/CSSEGISandData/COVID-19/commit/4360e50239b4eb6b22f3a1759323748f36752177) since 2023-03-10.

For historical view, monthly charts for the first couple years of the COVID-19 pandemic will be continue to be available at [coviz.michaelaltfield.net](https://coviz.michaelaltfield.net)

 * [https://coviz.michaelaltfield.net](https://coviz.michaelaltfield.net)

----

This is the website repo for building the coviz.org website using GitLab Pages. 

Check the resulting website here: <https://www.coviz.org>

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in the file [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

GitLab comes with 2,000 free minutes of execution on Google Cloud per month. Thuis "just works" when you `git push` to master. Alternatively, you can also test your build locally on your workstation before pushing to save these minutes. Here's how:

### Build the pelican site

This section will describe how you can build the pelican site on your workstation and browse to it in your browser on http://127.0.0.1:8000/

#### Using Apt-get

Run the following commands to build this website's static content on linux (tested on Debian 9 & 10):

```
git clone git@gitlab.com:coviz-org/pelican.git
cd pelican

# update the dataset
git submodule sync
git submodule update --init
pushd data/jhcsse/; git checkout master; git pull; popd

sudo apt-get install pelican python3-qrcode python3-plotly

make clean && make html
pushd public; python3 -m http.server --bind 127.0.0.1; popd
```
You can now view the static site locally on your machine with a browser at http://127.0.0.1:8000/

Note that we don't use `make devserver` as it causes an issue:

```
pelican: error: unrecognized arguments: -lr
```

...because Debian's pelican version is currently only v3.7.1, but `-l` == `--listen` wasn't added until v4.0.0

#### Using Pip

Alternatively, for a newer version of Pelican, you can run (tested in Debian 10):

```
git clone git@gitlab.com:coviz-org/pelican.git
cd pelican

# update the dataset
git submodule sync
git submodule update --init
pushd data/jhcsse/; git checkout master; git pull; popd

apt-get install -y python3 python3-pip xvfb libxcomposite1 libxcursor1 libxi6 libxtst6 libnss3 libgdk-pixbuf2.0 libgtk-3-0 libxss1 libasound2 curl unzip git nodejs npm
pip3 install -r requirements.txt

sudo npm install -g --unsafe-perm=true electron orca

make clean && make devserver
```

You can now view the static site locally on your machine with a browser at http://127.0.0.1:8000/

Read more at Pelican's [documentation].

### Generate the pelican site with docker

You can mimic the entire build that happens on google cloud on your local workstation by running these commands (tested in Debian 10):

```
git clone git@gitlab.com:coviz-org/pelican.git
cd pelican

sudo apt-get -y install docker.io gitlab-runner
sudo docker login # first signup for hub.docker.com (this may or may not be necessary)

# fix a tagging issue pulling the latest gitlab-runner-helper image
#  * https://gitlab.com/gitlab-org/gitlab-runner/issues/4773
sudo docker pull gitlab/gitlab-runner-helper:x86_64-latest
sudo docker tag gitlab/gitlab-runner-helper:x86_64-latest gitlab-runner-helper:12.8.0

# build it!
sudo gitlab-runner exec docker pages
```
## Updating dependencies in our docker file

In order to build graphs that export to both html/js files *and* png images, we're using pythin with plotly. This process requires a *lot* of dependencies, including python, plotly, nodejs, electron, many X11 libraries, etc. To cut down the time it takes to build this site, we publish a "fully-baked" docker base image so the GitLab CI process running on Google Cloud to generate our coviz.org website doesn't have to install any dependencies. It only has to download a ~1.3G base docker image (~600M compressed).

This docker image can be found here: <https://hub.docker.com/r/covizorg/node-13-buster-slim>

The above-linked docker image can be updated by running the following commands (tested in Debian 10):

```
git clone git@gitlab.com:covidspread/pelican.git
cd pelican

sudo apt-get -y install docker.io
sudo docker login

# edit the Dockerfile with your changes as needed
vim Dockerfile

# build & upload the changes to the image
sudo docker build -t covizorg/node-13-buster-slim .
sudo docker push covizorg/node-13-buster-slim

# don't forget to edit & push your changes!
git commit -a
git push
```

## License

All sourcecode found in this repository is copyright Michael Altfield and released under the GNU AGPLv3 license. For more information, please see [`LICENSE`](LICENSE).

All content produced from the code (ie: charts, pictures, etc) is copyright Michael Altfield and released under the CC-BY-SA license. For more information, see http://creativecommons.org/licenses/by-sa/4.0/
