#!/usr/bin/env python3
################################################################################
# File:    plugins/coviz_models/coviz_models.py
# Version: 0.8
# Purpose: Pelican plugin for generating plotly html/js and png images of charts
#          that predict the spread of coronavirus. Fore more info, see:
#           * https://coviz.michaelaltfield.net
# Authors: Michael Altfield <michael@michaelaltfield.net>
# Authors: Michael Goldenberg
# Created: 2020-04-18
# Updated: 2024-06-07
################################################################################

################################################################################
#                                   IMPORTS                                    #
################################################################################

from pelican import signals, contents

from datetime import datetime
from datetime import timedelta
from logging import warning, info
from pytz import timezone
import os.path, sys, random

import csv
from plotly import graph_objs as go
import numpy as np

################################################################################
#                                  SETTINGS                                    #
################################################################################

# path to csv database
DATA_FILE_PATH = os.path.join( os.getcwd(), 'data', 'jhcsse', 'csse_covid_19_data', 'csse_covid_19_time_series', 'time_series_covid19_confirmed_global.csv' )

# list of regions for graphs to generate
#REGIONS = [ "earth" ]
#REGIONS = [ "earth", "Mexico" ]
#REGIONS = [ "earth", "US", "Mexico", "Denmark" ]
#REGIONS = [ "earth", "United Kingdom", "Spain", "Canada" ]
#REGIONS = [ "earth", "Italy", "Spain", "US", "Denmark", "India", "Ireland", "United Kingdom", "France", "Brazil", "Canada" ]
#REGIONS = [ "earth", "Italy", "Spain", "US", "Denmark", "India", "Ireland", "United Kingdom", "France", "Brazil", "Canada", "Nepal", "Thailand", "New Zealand", "Germany", "Panama", "Australia", "Portugal", "Japan", "Argentina", "Iran", "Chile", "Turkey", "Russia", "Switzerland" ]
#REGIONS = [ "earth", "Italy", "Spain", "US", "Denmark", "India", "Ireland", "United Kingdom", "France", "Brazil", "Canada", "Nepal", "Thailand", "New Zealand", "Germany", "Panama", "Australia", "Portugal", "Japan", "Argentina", "Iran", "Chile", "Turkey", "Russia", "Finland", "Switzerland", "Poland", "Singapore", "Indonesia", "Philippines", "China", "Sweden", "Peru", "Mexico", "Ukraine", "Algeria", "Slovenia", "Iceland", "Ecuador", "Colombia",  "El Salvador", "Honduras", "Suriname", "Dominican Republic", "Morocco", "Libya", "South Africa", "Namibia", "Djibouti", "Yemen", "Saudi Arabia", "Oman", "Qatar", "Bolivia", "Venezuela" ]
#REGIONS = [ "earth", "Italy", "Spain", "US", "India", "Ireland", "United Kingdom", "France", "Brazil", "Canada", "Nepal", "Thailand", "New Zealand", "Germany", "Panama", "Australia", "Japan", "Argentina", "Iran", "Turkey", "Russia", "Finland", "Singapore", "Philippines", "China", "Peru", "Mexico", "Algeria", "Slovenia", "Iceland", "Ecuador", "Colombia",  "El Salvador", "Honduras", "Suriname", "Dominican Republic", "Morocco", "Libya", "South Africa", "Namibia", "Yemen", "Saudi Arabia", "Oman", "Qatar", "Bolivia", "Venezuela" ]
REGIONS = [ "earth", "Italy", "Spain", "US", "Denmark", "India", "Ireland", "United Kingdom", "France", "Brazil", "Canada", "Nepal", "Thailand", "New Zealand", "Germany", "Panama", "Australia", "Portugal", "Japan", "Argentina", "Iran", "Chile", "Turkey", "United Arab Emirates", "Russia", "Finland", "Switzerland", "Poland", "Singapore", "Vietnam", "Ecuador" ]

# set this to False to skip the generation of PNG images (speeds up
# iteration when just working on the html/js graph)
#GENERATE_PNG = False
GENERATE_PNG = True

HISTORY_START = 76
#HISTORY_START = 372

# after this many days back, we'll only keep the charts generated on the first of every month
KEEP_ALL_PAGES_THIS_MANY_DAYS_BACK = 7

rssDoublings = dict()

################################################################################
#                                  FUNCTIONS                                   #
################################################################################

def country_to_code( country ):

	countryDict = {'Afghanistan': 'AF',
	 'Albania': 'AL',
	 'Algeria': 'DZ',
	 'American Samoa': 'AS',
	 'Andorra': 'AD',
	 'Angola': 'AO',
	 'Anguilla': 'AI',
	 'Antarctica': 'AQ',
	 'Antigua and Barbuda': 'AG',
	 'Argentina': 'AR',
	 'Armenia': 'AM',
	 'Aruba': 'AW',
	 'Australia': 'AU',
	 'Austria': 'AT',
	 'Azerbaijan': 'AZ',
	 'Bahamas': 'BS',
	 'Bahrain': 'BH',
	 'Bangladesh': 'BD',
	 'Barbados': 'BB',
	 'Belarus': 'BY',
	 'Belgium': 'BE',
	 'Belize': 'BZ',
	 'Benin': 'BJ',
	 'Bermuda': 'BM',
	 'Bhutan': 'BT',
	 #'Bolivia, Plurinational State of': 'BO',
	 'Bolivia': 'BO',
	 'Bonaire, Sint Eustatius and Saba': 'BQ',
	 'Bosnia and Herzegovina': 'BA',
	 'Botswana': 'BW',
	 'Bouvet Island': 'BV',
	 'Brazil': 'BR',
	 'British Indian Ocean Territory': 'IO',
	 'Brunei Darussalam': 'BN',
	 'Bulgaria': 'BG',
	 'Burkina Faso': 'BF',
	 'Burundi': 'BI',
	 'Cambodia': 'KH',
	 'Cameroon': 'CM',
	 'Canada': 'CA',
	 'Cape Verde': 'CV',
	 'Cayman Islands': 'KY',
	 'Central African Republic': 'CF',
	 'Chad': 'TD',
	 'Chile': 'CL',
	 'China': 'CN',
	 'Christmas Island': 'CX',
	 'Cocos (Keeling) Islands': 'CC',
	 'Colombia': 'CO',
	 'Comoros': 'KM',
	 'Congo': 'CG',
	 'Congo, the Democratic Republic of the': 'CD',
	 'Cook Islands': 'CK',
	 'Costa Rica': 'CR',
	 'Country name': 'Code',
	 'Croatia': 'HR',
	 'Cuba': 'CU',
	 'Curaçao': 'CW',
	 'Cyprus': 'CY',
	 'Czech Republic': 'CZ',
	 "Côte d'Ivoire": 'CI',
	 'Denmark': 'DK',
	 'Djibouti': 'DJ',
	 'Dominica': 'DM',
	 'Dominican Republic': 'DO',
	 'Ecuador': 'EC',
	 'Egypt': 'EG',
	 'El Salvador': 'SV',
	 'Equatorial Guinea': 'GQ',
	 'Eritrea': 'ER',
	 'Estonia': 'EE',
	 'Ethiopia': 'ET',
	 'Falkland Islands (Malvinas)': 'FK',
	 'Faroe Islands': 'FO',
	 'Fiji': 'FJ',
	 'Finland': 'FI',
	 'France': 'FR',
	 'French Guiana': 'GF',
	 'French Polynesia': 'PF',
	 'French Southern Territories': 'TF',
	 'Gabon': 'GA',
	 'Gambia': 'GM',
	 'Georgia': 'GE',
	 'Germany': 'DE',
	 'Ghana': 'GH',
	 'Gibraltar': 'GI',
	 'Greece': 'GR',
	 'Greenland': 'GL',
	 'Grenada': 'GD',
	 'Guadeloupe': 'GP',
	 'Guam': 'GU',
	 'Guatemala': 'GT',
	 'Guernsey': 'GG',
	 'Guinea': 'GN',
	 'Guinea-Bissau': 'GW',
	 'Guyana': 'GY',
	 'Haiti': 'HT',
	 'Heard Island and McDonald Islands': 'HM',
	 'Holy See (Vatican City State)': 'VA',
	 'Honduras': 'HN',
	 'Hong Kong': 'HK',
	 'Hungary': 'HU',
	 'ISO 3166-2:GB': '(.uk)',
	 'Iceland': 'IS',
	 'India': 'IN',
	 'Indonesia': 'ID',
	 #'Iran, Islamic Republic of': 'IR',
	 'Iran': 'IR',
	 'Iraq': 'IQ',
	 'Ireland': 'IE',
	 'Isle of Man': 'IM',
	 'Israel': 'IL',
	 'Italy': 'IT',
	 'Jamaica': 'JM',
	 'Japan': 'JP',
	 'Jersey': 'JE',
	 'Jordan': 'JO',
	 'Kazakhstan': 'KZ',
	 'Kenya': 'KE',
	 'Kiribati': 'KI',
	 "Korea, Democratic People's Republic of": 'KP',
	 #'Korea, Republic of': 'KR',
	 'South Korea': 'KR',
	 'Kuwait': 'KW',
	 'Kyrgyzstan': 'KG',
	 "Lao People's Democratic Republic": 'LA',
	 'Latvia': 'LV',
	 'Lebanon': 'LB',
	 'Lesotho': 'LS',
	 'Liberia': 'LR',
	 'Libya': 'LY',
	 'Liechtenstein': 'LI',
	 'Lithuania': 'LT',
	 'Luxembourg': 'LU',
	 'Macao': 'MO',
	 'Macedonia, the former Yugoslav Republic of': 'MK',
	 'Madagascar': 'MG',
	 'Malawi': 'MW',
	 'Malaysia': 'MY',
	 'Maldives': 'MV',
	 'Mali': 'ML',
	 'Malta': 'MT',
	 'Marshall Islands': 'MH',
	 'Martinique': 'MQ',
	 'Mauritania': 'MR',
	 'Mauritius': 'MU',
	 'Mayotte': 'YT',
	 'Mexico': 'MX',
	 'Micronesia, Federated States of': 'FM',
	 'Moldova, Republic of': 'MD',
	 'Monaco': 'MC',
	 'Mongolia': 'MN',
	 'Montenegro': 'ME',
	 'Montserrat': 'MS',
	 'Morocco': 'MA',
	 'Mozambique': 'MZ',
	 'Myanmar': 'MM',
	 'Namibia': 'NA',
	 'Nauru': 'NR',
	 'Nepal': 'NP',
	 'Netherlands': 'NL',
	 'New Caledonia': 'NC',
	 'New Zealand': 'NZ',
	 'Nicaragua': 'NI',
	 'Niger': 'NE',
	 'Nigeria': 'NG',
	 'Niue': 'NU',
	 'Norfolk Island': 'NF',
	 'Northern Mariana Islands': 'MP',
	 'Norway': 'NO',
	 'Oman': 'OM',
	 'Pakistan': 'PK',
	 'Palau': 'PW',
	 #'Palestine, State of': 'PS',
	 'Palestine': 'PS',
	 'Panama': 'PA',
	 'Papua New Guinea': 'PG',
	 'Paraguay': 'PY',
	 'Peru': 'PE',
	 'Philippines': 'PH',
	 'Pitcairn': 'PN',
	 'Poland': 'PL',
	 'Portugal': 'PT',
	 'Puerto Rico': 'PR',
	 'Qatar': 'QA',
	 'Romania': 'RO',
	 #'Russian Federation': 'RU',
	 'Russia': 'RU',
	 'Rwanda': 'RW',
	 'Réunion': 'RE',
	 'Saint Barthélemy': 'BL',
	 'Saint Helena, Ascension and Tristan da Cunha': 'SH',
	 'Saint Kitts and Nevis': 'KN',
	 'Saint Lucia': 'LC',
	 'Saint Martin (French part)': 'MF',
	 'Saint Pierre and Miquelon': 'PM',
	 'Saint Vincent and the Grenadines': 'VC',
	 'Samoa': 'WS',
	 'San Marino': 'SM',
	 'Sao Tome and Principe': 'ST',
	 'Saudi Arabia': 'SA',
	 'Senegal': 'SN',
	 'Serbia': 'RS',
	 'Seychelles': 'SC',
	 'Sierra Leone': 'SL',
	 'Singapore': 'SG',
	 'Sint Maarten (Dutch part)': 'SX',
	 'Slovakia': 'SK',
	 'Slovenia': 'SI',
	 'Solomon Islands': 'SB',
	 'Somalia': 'SO',
	 'South Africa': 'ZA',
	 'South Georgia and the South Sandwich Islands': 'GS',
	 'South Sudan': 'SS',
	 'Spain': 'ES',
	 'Sri Lanka': 'LK',
	 'Sudan': 'SD',
	 'Suriname': 'SR',
	 'Svalbard and Jan Mayen': 'SJ',
	 'Swaziland': 'SZ',
	 'Sweden': 'SE',
	 'Switzerland': 'CH',
	 'Syrian Arab Republic': 'SY',
	 #'Taiwan, Province of China': 'TW',
	 'Taiwan': 'TW',
	 'Tajikistan': 'TJ',
	 'Tanzania, United Republic of': 'TZ',
	 'Thailand': 'TH',
	 'Timor-Leste': 'TL',
	 'Togo': 'TG',
	 'Tokelau': 'TK',
	 'Tonga': 'TO',
	 'Trinidad and Tobago': 'TT',
	 'Tunisia': 'TN',
	 'Turkey': 'TR',
	 'Turkmenistan': 'TM',
	 'Turks and Caicos Islands': 'TC',
	 'Tuvalu': 'TV',
	 'Uganda': 'UG',
	 'Ukraine': 'UA',
	 #'United Arab Emirates': 'AE',
	 'UAE': 'AE',
	 'United Kingdom': 'GB',
	 'United States': 'US',
	 'United States Minor Outlying Islands': 'UM',
	 'Uruguay': 'UY',
	 'Uzbekistan': 'UZ',
	 'Vanuatu': 'VU',
	 #'Venezuela, Bolivarian Republic of': 'VE',
	 'Venezuela': 'VE',
	 #'Viet Nam': 'VN',
	 'Vietnam': 'VN',
	 'Virgin Islands, British': 'VG',
	 'Virgin Islands, U.S.': 'VI',
	 'Wallis and Futuna': 'WF',
	 'Western Sahara': 'EH',
	 'Yemen': 'YE',
	 'Zambia': 'ZM',
	 'Zimbabwe': 'ZW',
#	 'Åland Islands': 'AX'}
	 'Åland Islands': 'AX',
# custom shit here
	 'US': 'US',
	 'earth': 'EARTH' }

	result = countryDict.get(country)

	if result == None:

		for key,value in countryDict.items():
			if value.lower() == country:
				return key

	if result == None:
		return ''

	return result.lower()

# takes a list of items and splits them into 4 lists such that they contain
# an even number of items, with extra items spilling-over evenly to the first
# lists in-order
def split_to_four_boxes( items ):

	# calculate how many items we should put in each div
	# (this is ridiculious, but it works)
	itemsPerDiv = int( len(items) / 4 )

	if (len(items)/4) == itemsPerDiv:
		# divides eveny into 4 divs; perfect!
		firstItemDiv2 = itemsPerDiv
		firstItemDiv3 = itemsPerDiv*2
		firstItemDiv4 = itemsPerDiv*3
	else:
		firstItemDiv2 = itemsPerDiv+1

		itemsRemaining = len(items) - firstItemDiv2
		itemsPerDiv = int( itemsRemaining / 3 )
		if (itemsRemaining/3) == itemsPerDiv:
			# divides eveny into 3 remaining divs; perfect!
			firstItemDiv3 = firstItemDiv2 + itemsPerDiv*1
			firstItemDiv4 = firstItemDiv3 + itemsPerDiv
		else: 
			firstItemDiv3 = firstItemDiv2 + itemsPerDiv + 1

			itemsRemaining = len(items) - firstItemDiv3
			itemsPerDiv = int( itemsRemaining / 2 )
			if (itemsRemaining/2) == itemsPerDiv:
			# divides eveny into 3 remaining divs; perfect!
				firstItemDiv4 = firstItemDiv3 + itemsPerDiv
			else: 
				firstItemDiv4 = firstItemDiv3 + itemsPerDiv + 1

	return [
	 items[0:firstItemDiv2],
	 items[firstItemDiv2:firstItemDiv3],
	 items[firstItemDiv3:firstItemDiv4],
	 items[firstItemDiv4:]
	]

# like x_to_xaxsis_label() but swaps January 1 with the year
# this was added after COVID lasted longer than 1 year :(
def x_to_xaxis_label2( x ):

	xDates = []

	for i in x:
		date = x_to_date_label(i)

		# is this Jan 1?
		if date.strftime("%m-%d") == "01-01":
			# this is the first of the new year; just display the year
			xDates.append( str(x_to_date_label(i).strftime("%Y")) )
		else:
			# this is not the first of the year; just display the month & day
			xDates.append( str(x_to_date_label(i).strftime("%b %d")) )

	return xDates

# takes-in a list of x values and returns a list converted to human-readable
# dates (of type string) that should be displayed on the x-axis
def x_to_xaxis_label( x ):

	xDates = []

	for i in x:
		xDates.append( str(x_to_date_label(i).strftime("%Y-%m-%d")) )

	return xDates

# takes-in a given x and returns the cooresponding datetime object that can be
# used with strftime to print a human-readable date for the given time on the
# x-axis
def x_to_date_label( x ):

	start = datetime.strptime( "2020-01-22 UTC", "%Y-%m-%d %Z" )
	return start + timedelta(days=x)

#def add_model_pages( generator, metadata ):
	# TODO: figure out how to use this

def get_generators(generators):
	return CovizModelsGenerator

def register():
	#signals.page_generator_context.connect( add_model_pages )
	signals.get_generators.connect(get_generators)

def shouldWeHavePageForThisDay( thisChartday, lastChartday ):

	if lastChartday-thisChartday < KEEP_ALL_PAGES_THIS_MANY_DAYS_BACK:
		return True

	# is this day the first of the month?
	if int( x_to_date_label(thisChartday).strftime("%d") ) == 1:
		return True

	return False

def generate_base_graph( y, x_start_extrapolation, hypotheticalAnnotation ):

	#####################
	# DECLARE VARIABLES #
	#####################

	fig = go.Figure()

	x = [i for i in range(len(y))]

	fig.add_trace(go.Scatter(
	 #x = x,
	 #x=list(map( lambda x: x_to_date_label(x).strftime("%Y-%m-%d"), x)),
	 x = x_to_xaxis_label( x ),
	 y=y, 
	 line_shape='spline',
	 name="Actual",
	 showlegend=False
	))

	# draw the vertical line dividing historical data from hypothetical data
	fig.layout.update( shapes=[dict(
	 type = 'line',
	 yref = 'paper',
	 y0= 0, y1= 1,
	 xref = 'x',
	 # draw this line on the x-axis where the data ends, and feed plotly the
	 # date format it needs (hack to convert to a list and back again to a
	 # single point on the x-axis)
	 x0 = x_to_xaxis_label( [len(x)-1] ).pop(),
	 x1 = x_to_xaxis_label( [len(x)-1] ).pop(),
	 line = dict(
	  color = 'red',
	  ),
	)] )

	fig.add_annotation(
	 # draw this line on the x-axis where the data ends, and feed plotly the
	 # date format it needs (hack to convert to a list and back again to a
	 # single point on the x-axis)
	 xanchor = "left",
	 x = x_to_xaxis_label( [len(x)-1] ).pop(),
	 y = 1,
	 yref = "paper",
	 yanchor = "top",
	 text = hypotheticalAnnotation,
	 showarrow = False,
	 textangle = -90,
	)

	# Add images
	fig.add_layout_image( dict(
	 source="https://coviz.michaelaltfield.net/images/covizlogo_20200410_7/coviz-rectangle_200.png",
	 xref="paper", yref="paper",
	 xanchor="left", yanchor="top",
	 x=0.03, y=0.8,
	 sizex=0.15,
	 sizey=0.15,
	 opacity=0.5,
	 #sizing="stretch",
	 #layer="below"
	) )

	fig.update_layout(
	 #xaxis_title = "Days since Jan 22nd",
	 xaxis_tickformat = "%b %d\n%Y",
	 margin = dict( l=20, r=20, t=70, b=0 ),
	 legend = dict(
	  orientation = "h",
	  font = dict( family='Liberation Sans' ),
	 ),
	 font = dict( family='Liberation Sans' ),
	)

	return fig

def graph_add_title( fig, title ): 

	fig.update_layout(
	 title = {
	 'text': title,
	 'xanchor': "center",
	 'yanchor': "top",
	 'x': 0.5,
	 'font': { 'family': 'Liberation Sans' },
	 },
	)

	return fig

def graph_add_top_left_annotation( fig, topLeftAnnotation ): 

	fig.add_annotation(
	 x = 0, xref = "paper",
	 y = 1, yref = "paper",
	 text = str(topLeftAnnotation),
	 align = "left",
	 showarrow = False,
	)

	return fig

# moves the "Hypothetical Data * ..." annotation to the top or bottom
# as needed, so that it does not write on-top of the plot lines
def graph_move_hypothetical_annotation( fig ): 

	# determine the max value of y on this chart
	max_y = 0
	for data in fig.data:
		for y in data.y:
			if y > max_y:
				max_y = y

	# determine the value of y on the boundry between historical & hypothetical
	# data plots
	last_y = len( fig.data[0].x ) - 1
	last_y_value = fig.data[0].y[last_y]

	# is the intersection of the historical line and the red "hypothetical data"
	# line above or below half-way up the y axis?
	if last_y_value / max_y > 0.5:
		# the intersection occurs on the top-half of the graph;
		# move the annotation to the bottom-half to prevent them from overlapping

		for annotation in fig.layout.annotations:
			if "Hypothetical Data *" in annotation.text:
				annotation.y = 0
				annotation.yanchor = "bottom"

	return fig

def add_hypothetical_e2a ( fig ):

	y = fig.data[0].y
	x = [i for i in range(len(y))]

	# extrapolate data with various contexts, i.e. 3, 7, 30 day history
	for i in [3, 7, 30]:

		extrapolate = np.poly1d(np.polyfit(x[-i:], y[-i:], 2))

		x_extrapolated = np.linspace(len(x) - 1, len(x) + 30, 30)
		y_extrapolated = extrapolate(x_extrapolated)
		fig.add_trace(go.Scatter(
		 		x = x_to_xaxis_label( x_extrapolated ),
      		y=y_extrapolated, 
      		line_shape='spline',
      		line=dict(dash='dot'),
      		name="Last {} Days".format(i)
  		))

def add_hypothetical_e2b ( fig ):

	y = fig.data[0].y
	x = [i for i in range(len(y))]

	# extrapolate data with various contexts, i.e. 3, 7, 30 day history
	for i in [3, 7, 30]:

		extrapolate = np.poly1d(np.polyfit(x[-i:], y[-i:], 2))

		def monotonic_extrapolate(x):
			monotonic_extrapolate.last = max(
				extrapolate(x), 
				monotonic_extrapolate.last
			)
			return monotonic_extrapolate.last
		monotonic_extrapolate.last = 0 

		x_extrapolated = [i for i in range(len(x) - 1, len(x) + 30)]
		y_extrapolated = list(map(monotonic_extrapolate, x_extrapolated))
		fig.add_trace(go.Scatter(
		 		x = x_to_xaxis_label( x_extrapolated ),
      		y=y_extrapolated, 
      		line_shape='spline',
      		line=dict(dash='dot'),
      		name="Last {} Days".format(i)
  		))

def add_hypothetical_d1 ( fig, countryCode ):

	doublings = list()
	rssDoublings[countryCode] = list()

	y = fig.data[0].y
	x = [i for i in range(len(y))]

	# there's no MAXINT in python3; this is big enough
	earliestDoubling = 999999999

	# DETERMINE WHERE THE END OF THE PROJECTION SHOULD BE

	# is the current number of cases less than 125,000?
	currentCases = y[ len(y)-1 ]
	extraDays = 30
	if currentCases > 1000:
		# only go more than 30-days out if there's more than 1,000 cases already

		nextDoubling = 1000/2
		for i,j in enumerate(y):
			if j >= nextDoubling*2:

				nextDoubling *= 2

				doublings.append( dict() )
				doublings[ len(doublings)-1 ]['count'] = nextDoubling
				doublings[ len(doublings)-1 ]['x'] = i
				doublings[ len(doublings)-1 ]['xlabel'] = x_to_xaxis_label( [i] ).pop()
				if len(doublings)-1 == 0:
					# the delta for our first entry isn't calculated
					doublings[ len(doublings)-1 ]['delta'] = '-'
				else:
					doublings[ len(doublings)-1 ]['delta'] = i - doublings[ len(doublings)-2 ]['x']

				# is the doubling for our current day (today)?
				if len(x)-i < KEEP_ALL_PAGES_THIS_MANY_DAYS_BACK:
					rssDoublings[countryCode].append( doublings[ len(doublings)-1 ] )

				# draw tick marks at every doubling
				fig.add_shape(dict(
				 type = 'line',
				 yref = 'y',
				 y0= 0, y1= nextDoubling,
				 xref = 'x',
				 # draw this line on the x-axis where the data ends, and feed plotly the
				 # date format it needs (hack to convert to a list and back again to a
				 # single point on the x-axis)
				 x0 = x_to_xaxis_label( [i] ).pop(),
				 x1 = x_to_xaxis_label( [i] ).pop(),
				 line = dict(
				  color = 'black',
				  ),
				))
				fig.add_shape(dict(
				 type = 'line',
				 yref = 'paper',
				 y0= 0, y1= 1,
				 xref = 'x',
				 # draw this line on the x-axis where the data ends, and feed plotly the
				 # date format it needs (hack to convert to a list and back again to a
				 # single point on the x-axis)
				 x0 = x_to_xaxis_label( [i] ).pop(),
				 x1 = x_to_xaxis_label( [i] ).pop(),
				 line = dict(
				  color = 'black',
				  ),
				 opacity = 0.1
				))

		nextDoubling *= 2

		# there's no MAXINT in python3; this is big enough
		earliestDoubling = 999999999

		# add a new item to the end of our doublings list that itself will be a
		# list of all possible occurances of the next doubling (hypothetical)
		doublings.append( list() )
		rssDoublings[countryCode].append( list() )

		for i in [3, 7, 30]:

			# calculate the x for when y crosses the next doubling point by
			# finding the roots of the function, excluding complex/imaginary
			# numbers
			extrapolate = np.polyfit(x[-i:], y[-i:], 2)
			extrapolate[-1] -= nextDoubling
			fun = np.poly1d(extrapolate)
			roots = np.real(np.roots( extrapolate )).tolist()
			roots.sort()

			for root in roots:

				curDoubling = int(root)+1

				# is this day of the expected doubling of the current number of
				# cases after today and before 1-year from now?
				#if root > (len(y)-1) and root < (len(y)-1+365):
				#if root > (len(y)-1) and fun(curDoubling) > 0:
				# only use roots that cross the x-axis
				if root > (len(y)-1) and fun(curDoubling) > 0 and root < (len(y)-1+365):

					# record this potential doubling day for our table
					doublings[ len(doublings)-1 ].append( dict() )
					guessNum = len( doublings[ len(doublings)-1 ] )-1
					doublings[ len(doublings)-1 ][guessNum]['count'] = nextDoubling
					doublings[ len(doublings)-1 ][guessNum]['x'] = curDoubling
					doublings[ len(doublings)-1 ][guessNum]['xlabel'] = x_to_xaxis_label( [curDoubling] ).pop()
					doublings[ len(doublings)-1 ][guessNum]['delta'] = curDoubling - doublings[ len(doublings)-2 ]['x']

					# does this region have >1 doubling event in the past
					# <KEEP_ALL_PAGES_THIS_MANY_DAYS_BACK> days?
					if rssDoublings[countryCode][0] != list():
						# also add this to our global var for adding to the rss later
						rssDoublings[countryCode][ len(rssDoublings[countryCode])-1 ].append( doublings[ len(doublings)-1 ][guessNum] )

					# draw a dashed line indicating which trace intersects the double
					fig.add_shape(dict(
					 type = 'line',
					 yref = 'y',
					 y0= 0, y1= nextDoubling,
					 xref = 'x',
					 # draw this line on the x-axis where the data ends, and feed plotly the
					 # date format it needs (hack to convert to a list and back again to a
					 # single point on the x-axis)
					 x0 = x_to_xaxis_label( [curDoubling] ).pop(),
					 x1 = x_to_xaxis_label( [curDoubling] ).pop(),
					 line = dict(
					  color = 'black',
					  dash = 'dot',
					  ),
					))

					# let's mark this as the soonest estimated doubling day if it is.
					if curDoubling < earliestDoubling and fun(curDoubling) > 0:
						earliestDoubling = curDoubling

					# let's extrapolate through to this day if it's our highest
					# yet
					daysPastHypotheticalLine = int(root-len(y)+2)
					if daysPastHypotheticalLine > extraDays:
						extraDays = daysPastHypotheticalLine
						break

	# draw a rectangle for the area where we predict the next doubling will occur
	# skip this if no predictions could be found
	if earliestDoubling != 999999999:

		fig.add_shape(dict(
		 type = 'rect',
		 yref = 'paper',
		 y0= 0, y1= 1,
		 xref = 'x',
		 # draw this line on the x-axis where the data ends, and feed plotly the
		 # date format it needs (hack to convert to a list and back again to a
		 # single point on the x-axis)
		 x0 = x_to_xaxis_label( [earliestDoubling] ).pop(),
		 x1 = x_to_xaxis_label( [len(y)+extraDays] ).pop(),
		 fillcolor = 'black',
		 opacity = 0.1,
		))


	# extrapolate data with various contexts, i.e. 3, 7, 30 day history
	for i in [3, 7, 30]:

		extrapolate = np.poly1d(np.polyfit(x[-i:], y[-i:], 2))

		def monotonic_extrapolate(x):
			monotonic_extrapolate.last = max(
				extrapolate(x), 
				monotonic_extrapolate.last
			)
			return monotonic_extrapolate.last
		monotonic_extrapolate.last = 0 

		x_extrapolated = [i for i in range(len(x) - 1, len(x) + extraDays)]
		y_extrapolated = list(map(monotonic_extrapolate, x_extrapolated))
		fig.add_trace(go.Scatter(
 			x = x_to_xaxis_label( x_extrapolated ),
     			y=y_extrapolated, 
     			line_shape='spline',
     			line = dict(
				 dash = 'dot',
				),
     			name="Last {} Days".format(i),
  		))

	return doublings

def getDoublingTable_d1( doublings ):

	html = '''

<!-- BOUNDARY_BEGIN=D1_TABLE -->
<table class="m-table">
  <caption>Next Doubling</caption>
  <thead>
    <tr>
      <th># Cases</th>
      <th>Date</th>
      <th>Days since<br/>previous<br/>doubling</th>
    </tr>
  </thead>
'''

	for i,doubling in enumerate(doublings):

		# is this element in the list also a list
		if type(doubling) is not list:
			# this isn't a list; it's a historical doubling day

			# create a human-readable count number for the table
			count = doubling['count']
			if count < 1000000:
				# if it's less than a million, just output the number
				count = int(count)
			elif count >= 1000000 and count < 1000000000:
				count = str( count/1000000 ) + " million"
			else:
				count = str( count/1000000000 ) + " billion"

			html = html + '''
  <tbody>
    <tr>
      <th scope="row">''' +str( count )+ '''</th>
      <td>''' +str( doubling['xlabel'] )+ '''</td>
      <td>''' +str( doubling['delta'] )+ '''</td>
    </tr>
  </tbody>
'''

		else:
			# this doubling entry is a list; it's our list of hypothetical
			# doublings for the next doubling day

			# is the set of all potential next doubling days empty?
			if doubling == list():
				# there is no predicted next doubling day; output 'never'

				# create a human-readable count number for the table
				count = doublings[i-1]['count']*2
				if count < 1000000:
					# if it's less than a million, just output the number
					count = int(count)
				elif count >= 1000000 and count < 1000000000:
					count = str( count/1000000 ) + " million"
				else:
					count = str( count/1000000000 ) + " billion"

				html = html + '''

  <tfoot>
    <tr class="m-dim">
      <th>''' +str(count)+ '''</th>
      <td>never</td>
      <td>inf</td>
    </tr>
  </tfoot>
'''

			else:
				# the list is not empty; loop through all our potential/hypotehtical
				# doubling days

				earliestPotential = int()
				lastPotential = int()
				for j,potential in enumerate(doubling):
	
					# determine the potential doubling day that occurs first in time
					if potential['x'] < doublings[i][earliestPotential]['x'] \
					 or earliestPotential == None:
						earliestPotential = j
	
					# determine the potential doubling day that occurs last in time
					if potential['x'] > doublings[i][lastPotential]['x'] \
					 or lastPotential == None:
						lastPotential = j
	
				# create a human-readable count number for the table
				count = doubling[0]['count']
				if count < 1000000:
					# if it's less than a million, just output the number
					count = int(count)
				elif count >= 1000000 and count < 1000000000:
					count = str( count/1000000 ) + " million"
				else:
					count = str( count/1000000000 ) + " billion"

				# the output of the range of days should only display a range if
				# the two dates aren't the same :)
				if doubling[earliestPotential]['xlabel'] == doubling[lastPotential]['xlabel']:
					dayRange = str(doubling[earliestPotential]['xlabel'])
				else:
					dayRange = str(doubling[earliestPotential]['xlabel'])+ " -<br/>" +str(doubling[lastPotential]['xlabel'])

				# deltaRange
				if doubling[earliestPotential]['delta'] == doubling[lastPotential]['delta']:
					deltaRange = str(doubling[earliestPotential]['delta'])
				else:
					deltaRange = str(doubling[earliestPotential]['delta'])+ " - " +str(doubling[lastPotential]['delta'])
	
				html = html + '''
  <tfoot>
    <tr class="m-dim">
      <th>''' +str(count)+ '''</th>
      <td>''' +str(dayRange)+ '''</td>
      <td>''' +str(deltaRange)+ '''</td>
    </tr>
  </tfoot>
'''

	html = html + '''
</table>
<!-- BOUNDARY_END=D1_TABLE -->
'''

	return html

def graph_write_html( fig, output_dir_path, relative_dir_path, chartday_start, chartday_cur, chartday_latest, modelId, modelName, aboutSection ):

	relativeTwitterFilePath = relative_dir_path+ '/twitter.png'
	relativeFacebookFilePath = relative_dir_path+ '/facebook.png'
	output_html_dir_path = os.path.join( output_dir_path, relative_dir_path )
	output_html_file_path = os.path.join( output_html_dir_path, "index.html" )

	latestDateString = x_to_date_label( chartday_cur ).strftime( "%Y-%m-%d" )
	# is this the main models page (not in a date-specific subdir?)
	if latestDateString not in relative_dir_path:
		# we're writing the model-wide index.html file; link to the latest chart's twitter file
		relativeTwitterFilePath = relative_dir_path+ '/' +latestDateString+ '/twitter.png'
		relativeFacebookFilePath = relative_dir_path+ '/' +latestDateString+ '/facebook.png'
	
	# template
	# there's definitely a better way to do this. I tried to use the built-in
	# pelican writer.write_file(), but I couldn't figure out how to get the
	# template varible. I tried setting the template arg to 'page', but it
	# wants some object with a .render() function, which I wasted several
	# hours trying to find. Pelican documentation leaves a lot to be desired,
	# so for now I'm just doing this stupid hack
	preContentHtml = '<!DOCTYPE html><html lang="en" prefix="og: http://ogp.me/ns#"><head><meta charset="UTF-8" /><title>Model ' +modelId+ ' | Coviz</title><link rel="stylesheet" href="/static/m-light.css" /><link rel="stylesheet" href="/deps/css/coviz.css" /><link rel="icon" href="/favicon.ico" type="image/x-ico" /><link rel="canonical" href="/' +relative_dir_path+ '/index.html" /><link href="/feeds/all.atom.xml" type="application/atom+xml" rel="alternate" title="Coviz" /><meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="theme-color" content="#cb4b16" /><meta property="og:site_name" content="Coviz" /><meta property="og:title" content="' +modelName+ '" /><meta name="twitter:title" content="' +modelName+ '" /><meta property="og:url" content="/' +modelId+ '/index.html" /><meta property="og:description" content="Coviz is an open-source project which aims to generate and publish visualizations from data on the spread of coronavirus (COVID-19)" /><meta name="twitter:description" content="Coviz is an open-source project which aims to generate and publish visualizations from data on the spread of coronavirus (COVID-19)" /><meta name="twitter:card" content="summary_large_image" /><meta property="og:type" content="page" /><meta property="og:image" content="https://coviz.michaelaltfield.net/' +relativeFacebookFilePath+ '" /><meta name="twitter:image" content="https://coviz.michaelaltfield.net/' +relativeTwitterFilePath+ '" /><script defer src="https://www.googletagmanager.com/gtag/js?id=UA-163354069-1"></script><script defer src="/deps/js/ga/gtag.js"></script></head><body><header><nav id="navigation"><div class="m-container"><div class="m-row"><a href="/" id="m-navbar-brand" class="m-col-t-9 m-col-m-none m-left-m"><img src="/images/covizlogo_20200410_7/coviz-rectangle_200.png" /> </a><a id="m-navbar-show" href="#navigation" title="Show navigation" class="m-col-t-3 m-hide-m m-text-right"></a><a id="m-navbar-hide" href="#" title="Hide navigation" class="m-col-t-3 m-hide-m m-text-right"></a><div id="m-navbar-collapse" class="m-col-t-12 m-show-m m-col-m-none m-right-m"><div class="m-row"><ol class="m-col-t-6 m-col-m-none"><li><a href="/about/">About</a></li><li><a href="/models/" id="m-navbar-current">Models</a><ol><li><a href="/e2a/">e2a</a></li><li><a href="/e2b/">e2b</a></li><li><a href="/d1/">d1</a></li></ol></li></ol></li></ol><ol class="m-col-t-6 m-col-m-none" start="3"><li><a href="/blog/">Blog</a></li><li><a href="/donate/">Donate</a></li></ol></div></div></div></div></nav></header><main><article>'
	preContentHtml = preContentHtml + '''
<div class="m-container m-container-inflatable">
	<div class="m-row">
	<div class="m-col-l-10 m-push-l-1">
	<h1>''' +modelName+ '''</h1>

<!-- BOUNDARY_BEGIN=OLD_DATA_LINK -->
'''

	# if the user is viewing a stale page, warn them
	if chartday_cur < chartday_latest:

		# construct link so it works for region-specific subdirs too
		latestDateString = x_to_date_label( chartday_latest ).strftime( "%Y-%m-%d" )
		curLink = relative_dir_path.split( '/' )
		curLink[1] = latestDateString
		curLink = '/'.join(curLink)

		preContentHtml = preContentHtml + '''
<div class="m-note m-danger">
<p>You are currently viewing a graph of old data. To see the most current graph, please visit:</p> <ul><li><a href="/''' +curLink+ '">' +str( latestDateString )+ '''</a></li></ul>
</div>'''

	# otherwise, let's warn the user that this data is an archive
	else:

		preContentHtml = preContentHtml + '''
<div class="m-note m-danger">
<p><strong>Note: This website has been archived</strong>. Our data source of COVID-19 confirmed cases (John Hopkins University) <a href="https://github.com/CSSEGISandData/COVID-19/commit/4360e50239b4eb6b22f3a1759323748f36752177">stopped providing data</a> in March 2023.</p>

<p>Unfortunately, without incoming per-country data on COVID-19 positive test results, we cannot continue to predict the future spread of the virus during the ongoing pandemic. As a result, this website is being archived and the historical charts shown will be provided for archival purposes only.</p>
</div>'''

	preContentHtml = preContentHtml + '''
<!-- BOUNDARY_END=OLD_DATA_LINK -->

<p>Disclaimer: This page shows a <em>hypothetical</em> model attempting to predict the future spread of coronavirus. It was automatically generated using an <a href="/about/">open-source</a> algorithm. Such a model is inexact and may be wildly inaccurate. See below for more details.</p>

<p>For a list of other Extrapolation Models against the COVID-19 data, see our <a href="/models/">Models Page</a></p>
'''
	postContentHtml = '''
	<div class="m-button m-flat">
		<a href="/''' +relativeTwitterFilePath+ '''" style="padding: 0.2em 0.7em 0.2em 0.7em;" target="_blank"><span class="m-small">Download Graph</span></a>
	</div>
<div class="m-row">
<div class="m-button m-col-t-6">
	<a class="share facebook" href="https://www.facebook.com/sharer/sharer.php?u=https%3A//coviz.michaelaltfield.net/''' +relative_dir_path+ '''" target="_blank"><img src="/images/social/facebook-white.png" />share</a></div><div class="m-button m-col-t-6">
	<a class="share twitter" href="https://twitter.com/intent/tweet?text=Checkout%20this%20chart%20predicting%20the%20spread%20of%20coronavirus%20from%20past%20data&url=https%3A//coviz.michaelaltfield.net/''' +relative_dir_path+ '''&hashtags=dataviz,COVID-19,stayhome,FlattenTheCurve" target="_blank"><img src="/images/social/twitter-white.png" />tweet</a>
</div>
</div>

''' +aboutSection+ '''

<p>If you'd like to submit your own Extrapolation Model that fixes some of the issues above, see our <a href="/blog/covid-extrapolation/index.html">guide</a> to developing and submitting models.</p>'''

	# list links to other regions, if there are any
	if len(REGIONS) > 1:
		postContentHtml = postContentHtml + '''

<h2>Country-Specific Charts</h2>

<p>You can view graphs generated from data specific to individual countries below:</p>

'''
		regions = REGIONS.copy()
		if 'earth' in regions:
			regions.remove('earth')
		regions.sort()
		countryCodes = list()
		for region in regions:
			countryCodes.append( country_to_code(region).lower() )

		# calculate how many items we should put in each div
		# (this is ridiculious, but it works)
		itemsPerDiv = int( len(countryCodes) / 4 )

		if (len(countryCodes)/4) == itemsPerDiv:
			# divides eveny into 4 divs; perfect!
			firstItemDiv2 = itemsPerDiv
			firstItemDiv3 = itemsPerDiv*2
			firstItemDiv4 = itemsPerDiv*3
		else:
			firstItemDiv2 = itemsPerDiv+1

			itemsRemaining = len(countryCodes) - firstItemDiv2
			itemsPerDiv = int( itemsRemaining / 3 )
			if (itemsRemaining/3) == itemsPerDiv:
				# divides eveny into 3 remaining divs; perfect!
				firstItemDiv3 = firstItemDiv2 + itemsPerDiv*1
				firstItemDiv4 = firstItemDiv3 + itemsPerDiv
			else: 
				firstItemDiv3 = firstItemDiv2 + itemsPerDiv + 1

				itemsRemaining = len(countryCodes) - firstItemDiv3
				itemsPerDiv = int( itemsRemaining / 2 )
				if (itemsRemaining/2) == itemsPerDiv:
					# divides eveny into 3 remaining divs; perfect!
					firstItemDiv4 = firstItemDiv3 + itemsPerDiv
				else: 
					firstItemDiv4 = firstItemDiv3 + itemsPerDiv + 1

		postContentHtml = postContentHtml + '''
<div class="m-row">
<div class="m-col-l-3 m-col-m-5 m-col-s-5"><div class="m-note m-info">
<ul>
'''

		# loop through each of the countries for which we produce charts
		for i,country in enumerate(countryCodes):

			latestDateString = x_to_date_label( chartday_cur ).strftime( "%Y-%m-%d" )
			# is this the main models page (not in a date-specific subdir?)
			if latestDateString not in relative_dir_path:
				# we're writing the model-wide index.html file; link to the latest chart's countries
				countryLink = relative_dir_path+ '/' +latestDateString+ '/' + country
			else: 
				countryLink = relative_dir_path

				# remove the country if this is a country page linking to another country
				if countryLink.split('/').pop() != latestDateString:
					countryLink = '/'.join( relative_dir_path.split('/')[:-1] )

				countryLink = countryLink+ '/' +country

			# is this div full?
			if i in [firstItemDiv2, firstItemDiv3, firstItemDiv4]:
				# this div is full; create a new one
				postContentHtml = postContentHtml + '''
	</ul>
</div></div>'''

				if i == firstItemDiv3: 
					postContentHtml = postContentHtml + '''
<div class="m-clearfix-m"></div>
'''

				postContentHtml = postContentHtml + '''
<div class="m-col-l-3 m-col-m-5 m-col-s-5"><div class="m-note m-info">
	<ul>
'''

			postContentHtml = postContentHtml + '''
		<li>'''
			# don't add the <a>nchor link if it's the page we're currently on
			if relative_dir_path.split('/').pop() != country:
				postContentHtml = postContentHtml + '<a href="/''' +countryLink+ '/">'

			postContentHtml = postContentHtml + country_to_code(country)

			# don't add the <a>nchor link if it's the page we're currently on
			if relative_dir_path.split('/').pop() != country:
				postContentHtml = postContentHtml + '</a>'

			postContentHtml = postContentHtml + '''
		</li>'''

	postContentHtml = postContentHtml + '''
	</ul>
</div></div>
</div>
'''

	# only list the previous graphs if this is not our oldest graph :P
	if chartday_cur > chartday_start:
		postContentHtml = postContentHtml + '''

<h2>Previous Graphs</h2>

<p>You can view graphs generated from data on previous days below:</p>

'''

		# loop backwards from the current chart's day to the first chart we've made
		historyLinks=list()
		for day in range( chartday_cur-1, chartday_start-1, -1 ):

			# skip this iteration if the page for this day is too old
			# or not a monthly day
			if not shouldWeHavePageForThisDay( day, chartday_latest ):
				continue

			# construct link so it works for region-specific subdirs too
			curDateString = x_to_date_label( chartday_cur ).strftime( "%Y-%m-%d" )
			historyDateString = x_to_date_label( day ).strftime( "%Y-%m-%d" )

			if curDateString in relative_dir_path:
				historyLink = relative_dir_path.split( '/' )
				historyLink[1] = historyDateString
				historyLink = '/'.join(historyLink)
			else: 
				historyLink = relative_dir_path+ '/' + historyDateString

			dayString = str( x_to_date_label(day).strftime( "%Y-%m-%d" ) )
			#postContentHtml = postContentHtml + '<li>' \
			# +'<a href="/' +historyLink+ '">' +dayString+ '</a>' \
			# +'</li>'
			historyLinks.append( dayString )

		fourDivs = split_to_four_boxes( historyLinks )
		postContentHtml = postContentHtml + '''<div class="m-row">'''

		# DIV 1/4
		postContentHtml = postContentHtml + '''
	<div class="m-col-l-3 m-col-m-5 m-col-s-5"><div class="m-note m-default">
		<ul>
'''
		for item in fourDivs[0]:
			relative_dir_path_split = relative_dir_path.split('/')
			if len(relative_dir_path_split) == 3:
				# country-specific page url structure used here
				url = '/' +relative_dir_path_split[0]+ '/' +item+ '/' +relative_dir_path_split[2]
			else:
				# earth page url structure used here
				url = '/' +relative_dir_path_split[0]+ '/' +item

			postContentHtml = postContentHtml + "\n\t\t\t"

			postContentHtml = postContentHtml + '<li>' \
			 +'<a href="' +url+ '">' +item+ '</a>' \
			 +'</li>'
		postContentHtml = postContentHtml + '''
		</ul>
	</div></div>'''

		# DIV 2/4
		postContentHtml = postContentHtml + '''
	<div class="m-col-l-3 m-col-m-5 m-col-s-5"><div class="m-note m-default">
		<ul start="''' +str(1+len(fourDivs[0]))+ '''">
'''
		for item in fourDivs[1]:
			relative_dir_path_split = relative_dir_path.split('/')
			if len(relative_dir_path_split) == 3:
				# country-specific page url structure used here
				url = '/' +relative_dir_path_split[0]+ '/' +item+ '/' +relative_dir_path_split[2]
			else:
				# earth page url structure used here
				url = '/' +relative_dir_path_split[0]+ '/' +item
			postContentHtml = postContentHtml + "\n\t\t\t"
			postContentHtml = postContentHtml + '<li>' \
			 +'<a href="' +url+ '">' +item+ '</a>' \
			 +'</li>'
		postContentHtml = postContentHtml + '''
		</ul>
	</div></div>'''

		# DIV 3/4
		postContentHtml = postContentHtml + '''
	<div class="m-col-l-3 m-col-m-5 m-col-s-5"><div class="m-note m-default">
		<ul start="''' +str( 1+len(fourDivs[0])+len(fourDivs[1]) )+ '''">
'''
		for item in fourDivs[2]:
			relative_dir_path_split = relative_dir_path.split('/')
			if len(relative_dir_path_split) == 3:
				# country-specific page url structure used here
				url = '/' +relative_dir_path_split[0]+ '/' +item+ '/' +relative_dir_path_split[2]
			else:
				# earth page url structure used here
				url = '/' +relative_dir_path_split[0]+ '/' +item
			postContentHtml = postContentHtml + "\n\t\t\t"
			postContentHtml = postContentHtml + '<li>' \
			 +'<a href="' +url+ '">' +item+ '</a>' \
			 +'</li>'
		postContentHtml = postContentHtml + '''
		</ul>
	</div></div>'''

		# DIV 4/4
		postContentHtml = postContentHtml + '''
	<div class="m-col-l-3 m-col-m-5 m-col-s-5"><div class="m-note m-default">
		<ul start="''' +str( 1+len(fourDivs[0])+len(fourDivs[1])+len(fourDivs[2]) )+ '''">
'''
		for item in fourDivs[3]:
			relative_dir_path_split = relative_dir_path.split('/')
			if len(relative_dir_path_split) == 3:
				# country-specific page url structure used here
				url = '/' +relative_dir_path_split[0]+ '/' +item+ '/' +relative_dir_path_split[2]
			else:
				# earth page url structure used here
				url = '/' +relative_dir_path_split[0]+ '/' +item
			postContentHtml = postContentHtml + "\n\t\t\t"
			postContentHtml = postContentHtml + '<li>' \
			 +'<a href="' +url+ '">' +item+ '</a>' \
			 +'</li>'
		postContentHtml = postContentHtml + '''
		</ul>
	</div></div>'''

		postContentHtml = postContentHtml + '''
</div>'''

	postContentHtml = postContentHtml + '''

</div></div></div>
'''
	postContentHtml = postContentHtml +'</article></main><footer><nav><div class="m-container"><div class="m-row"><div class="m-col-s-3 m-col-t-6"><h3><a href="/">Coviz</a></h3><ul><li><a href="/about/">About</a></li><li><a href="/donate/">Donate</a></li><li><a href="/blog/">Blog</a></li></ul></div><div class="m-col-s-3 m-col-t-6"><h3>Legal</h3><ul><li><a href="/privacy-policy/">Privacy Policy</a></li><li><a href="/contact/">Contact</a></li></ul></div><div class="m-clearfix-t"></div><div class="m-col-s-3 m-col-t-6"><h3><a href="/models/">Models</a></h3><ul><li><a href="/e2a/">e2a</a></li><li><a href="/e2b/">e2b</a></li><li><a href="/d1/">d1</a></li></ul></div><div class="m-col-s-3 m-col-t-6"><h3>Social</h3><ul><li><a href="https://twitter.com/coviz_org">Twitter</a></li><li><a href="https://www.facebook.com/coviz.org">Facebook</a></li><li><a href="https://mastodon.social/@coviz_org">Mastodon</a></li><li><a href="https://gitlab.com/coviz-org/">Gitlab</a></li></ul></div></div><div class="m-row"><div class="m-col-l-10 m-push-l-1"><div style="display: flex; align-items: center; justify-content:center; flex-wrap:wrap;"><a href="/donate/"><img class="m-image" src="/images/ko-fi/BuyMeACoffee_blue-p-500.png" alt="Buy Me A Coffee" width="200" style="margin-bottom:0" /></a></div><br style="clear:both;" /><p>Copyright © 2020 <a href="https://www.michaelaltfield.net">Michael Altfield</a>. All rights reserved. Content licensed <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA</a>. Powered by <a href="https://getpelican.com">Pelican</a> and <a href="https://mcss.mosra.cz">m.css</a>.</p></div></div></div></nav></footer></body></html>'

	# create the destination dir
	info("creating model's dir")
	os.makedirs( os.path.join( output_html_dir_path ), exist_ok=True )

	fig.write_html(
	 str( output_html_file_path ),
	 dict( responsive = True ),
	 False,
	 '/deps/js/plotly/plotly.min.js',
	 False,
	 None,
	 False
	)

	noscript = '<noscript><a href="/' +relativeTwitterFilePath+ '"><img src="/' +relativeTwitterFilePath+ '" class="m-image" /></a></noscript>'

	# stupidly wrap this in the hard-coded HTML because I don't have time to
	# figure out how pelican wants this with context, writers, templates, etc
	graphsInHtml = '</div><div class="m-col-l-12 m-nopad">'
	with open( output_html_file_path, 'r') as f:
		graphsInHtml = graphsInHtml + str(f.read()) + noscript
	graphsInHtml = graphsInHtml + '</div><div class="m-col-l-10 m-push-l-1 m-nopadt">'

	# for the day's dir
	with open( output_html_file_path, 'w') as f:
		f.write( preContentHtml+ "\n" +graphsInHtml+ "\n" + postContentHtml )

def graph_write_twitter_png( fig, output_dir_path, relative_dir_path, topLeftAnnotation, bottomLeftAnnotation ):

	output_png_dir_path = os.path.join( output_dir_path, relative_dir_path )
	output_png_file_path = os.path.join( output_png_dir_path, "twitter.png" )

	# twitter images are 800 x 418 pixels, but they appear on a user's twitter feed (on
	# desktop) rescaled down to 507 x 265.433 pixels. Therefore, we must set the font
	# size to be visible at this smaller scale.
	twitterFontSizeSmall = 15
	twitterFontSizeMedium = 16

	# remove the top & bottom left annotations (we'll replace it with one that's
	# specific to twitter below
	newAnnotations = []
	for annotation in fig.layout.annotations:
		if annotation.text == topLeftAnnotation or annotation.text == bottomLeftAnnotation:
			continue
		newAnnotations.append( annotation )
	fig.layout.annotations = newAnnotations

	fig.add_annotation(
	 x = -0.04,
	 xref = "paper",
	 xanchor = "left",
	 y = -0.21,
	 yref = "paper",
	 yanchor = "bottom",
	 text = bottomLeftAnnotation,
	 align = "left",
	 showarrow = False,
	 font = dict( family='Liberation Sans', size=twitterFontSizeMedium, ),
	)

	# change the font size for the historical/hypothetical divider to small
	newAnnotations = []
	for annotation in fig.layout.annotations:
		if 'Hypothetical' in annotation.text:
			annotation.font = dict( family='Liberation Sans', size=twitterFontSizeSmall, )
		newAnnotations.append( annotation )
	fig.layout.annotations = newAnnotations

	fig.add_annotation(
	 x = 0, xref = "paper",
	 y = 1, yref = "paper",
	 text = str(topLeftAnnotation),
	 align = "left",
	 showarrow = False,
	)

	# make a few changes to the graph so it works well in our static twitter
	# card 800x418 px size

	fig.update_layout(
	 margin = dict( l=50, r=10, t=60, b=50, pad=0 ),
	 font = dict( family='Liberation Sans', size=twitterFontSizeMedium, ),
	 title = dict(
	 	font = dict( family='Liberation Sans', size=twitterFontSizeMedium, ),
	 ),
	 legend = dict(
	  	orientation = "h",
	   xanchor = "right",
		x = 1,
	 	font = dict( family='Liberation Sans', size=twitterFontSizeSmall, ),
	 ),
	)

	# does this plot's x-axis span 2 years?
	if '2021-01-01' in fig.data[0].x:
		# this plot's x-axis spans exactly 2 years; let's manually set the tick
		# values on the x-axis, so we can make it list the year instead of Jan 1st

		fig.update_xaxes(

		 # you have to use 'type' to override the 'date' formatting, but then it
		 # throws all the data out, unless the data were changed from dates to ints
		 #type = 'linear',
		 #tickmode = 'array',
		 #tickvals = x_tickvals,
		 #ticktext = x_ticktext,

		 # this is an easy cop-out to what I really want, which would be to replace
		 # Jan 1st with %Y. But that's too damn difficult in plotly, so we just
		 # display the 2-digit year on every month column -- which is more
		 # future-proof as well
		 tickformat = "%b",

		)

		fig.add_annotation(
		 x = "2021-01-01", xref = "x",
		 y = -0.096, yref = "paper",
		 text = str("Jan 2021"),
		 align = "left",
		 showarrow = False,
		 bgcolor = "#ffffff",
		)

#		fig.add_annotation(
#		 x = "0", xref = "paper",
#		 y = -0.096, yref = "paper",
#		 text = str("2020"),
#		 align = "left",
#		 showarrow = False,
#		 bgcolor = "#ffffff",
#		)

#
#		x_tickvals = list()
#		x_ticktext = list()
#
#		# loop through each of the would-be labels on the x-axis
#		# Note: This doesn't actually do anything for now (see work from 2021-01-28/29)
#		i = -1
#		for x in fig.data[0].x:
#			i += 1
#
#			# is this the 1st of the month?
#			year,month,day = x.split('-')
#			if day == '01' and month == '01':
#				# this is the 1st of the year. Add it to the chart's xaxis as the year only
#				value = datetime.strptime( year +"-"+ month +"-"+ day +" UTC", "%Y-%m-%d %Z" )
#				x_tickvals.append( value.strftime("%Y-%m-%d %Z") )
#				#x_tickvals.append( i )
#				x_ticktext.append( value.strftime("%Y") )
#			elif day == '01':
#				# this is the 1st of the month. Add it to the chart's xaxis labels as the month only
#				value = datetime.strptime( year +"-"+ month +"-"+ day +" UTC", "%Y-%m-%d %Z" )
#				x_tickvals.append( value.strftime("%Y-%m-%d %Z") )
#				#x_tickvals.append( i )
#				x_ticktext.append( value.strftime("%b") )
#
#		#print( "x_tickvals:|" +str(x_tickvals)+ "|" )
#		#print( "x_ticktext:|" +str(x_ticktext)+ "|" )

	else:
		# this chart is for only dates in the first year of the pandemic;
		# don't list the year

		fig.update_xaxes(
		 #dtick = "M1",
		 tickformat = "%b %d",
		)

	fig.update_xaxes(
	 # this is the only way to make the right-hand margin actually respected
	 automargin=False,
	 # prevent plotly from drawing tick labels at a 45 deg angle
	 tickangle=0,
	)

	if GENERATE_PNG:
		fig.write_image( output_png_file_path, width=800, height=418 )

	# remove twitter-specific annotations (we'll replace them with ones that
	# are facebook-specific below)
	newAnnotations = []
	for annotation in fig.layout.annotations:
		if annotation.text == topLeftAnnotation or annotation.text == bottomLeftAnnotation:
			continue
		newAnnotations.append( annotation )
	fig.layout.annotations = newAnnotations

def graph_write_facebook_png( fig, output_dir_path, relative_dir_path, topLeftAnnotation, bottomLeftAnnotation ):

	output_png_dir_path = os.path.join( output_dir_path, relative_dir_path )
	output_png_file_path = os.path.join( output_png_dir_path, "facebook.png" )

	# facebook images are 1,200 x 628 pixels, but they appear on a user's facebook feed
	# (on desktop) rescaled down to 474 x 247.95 pixels. Therefore, we must set the font
	# size to be visible at this smaller scale.
	facebookFontSizeSmall = 25
	facebookFontSizeMedium = 27

	fig.add_annotation(
	 x = -0.04,
	 xref = "paper",
	 xanchor = "left",
	 y = -0.20,
	 yref = "paper",
	 yanchor = "bottom",
	 text = bottomLeftAnnotation,
	 align = "left",
	 showarrow = False,
	 font = dict( family='Liberation Sans', size=facebookFontSizeMedium, ),
	)

	# change the font size for the historical/hypothetical divider to small
	newAnnotations = []
	for annotation in fig.layout.annotations:
		if 'Hypothetical' in annotation.text:
			annotation.font = dict( family='Liberation Sans', size=facebookFontSizeSmall, )
		newAnnotations.append( annotation )
	fig.layout.annotations = newAnnotations

	fig.add_annotation(
	 x = 0,
	 xref = "paper",
	 y = 1,
	 yref = "paper",
	 text = str(topLeftAnnotation),
	 align = "left",
	 showarrow = False,
	)

	fig.update_layout(
	 margin = dict( l=70, r=10, t=80, b=100, pad=0 ),
	 font = dict( family='Liberation Sans', size=facebookFontSizeMedium, ),
	 title = dict(
	 	font = dict( family='Liberation Sans', size=facebookFontSizeMedium, ),
	 ),
	 legend = dict(
	  	orientation = "h",
	   xanchor = "right",
		x = 1,
	 	font = dict( family='Liberation Sans', size=facebookFontSizeSmall, ),
	 ),
	)

	fig.update_xaxes(
	 # this is the only way to make the right-hand margin actually respected
	 automargin=False,
	 # prevent plotly from drawing tick labels at a 45 deg angle
	 tickangle=0,
	)

	if GENERATE_PNG:
		fig.write_image( output_png_file_path, width=1200, height=628 )

def generate_doubling_rss( output_dir_path, relative_dir_path, chartday_cur ):

	print( "INFO: updating " +str(relative_dir_path)+ "/feed.xml" )

	# bail if there's nothing to generate
	#if rssDoublings == [[]]:
	#		return

	rss_file_path = os.path.join( output_dir_path, relative_dir_path, 'feed.xml' )
	rss_build_date = datetime.utcnow().strftime("%d %b %Y %H:%M:%S")+ ' +0000'
	dateString = x_to_date_label( chartday_cur ).strftime( "%Y-%m-%d" )
	content = ''

	"""
	content = '''<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">

<channel>
	<atom:link href="https://coviz.michaelaltfield.net/d1/feed.xml" rel="self" type="application/rss+xml" />
	<title>Coviz Doubling</title>
	<link>https://coviz.michaelaltfield.net/d1/</link>
	<description>This d1 model mattempts to predict the future number of COVID-19 cases and estimate the next doubling of known cases.</description>
	<lastBuildDate>''' +rss_build_date+ '''</lastBuildDate>
	<language>en-US</language>'''
	"""

	# this is a dumb hack that shuffles the order of the dictionary keys
	# so they appear in the rss file in some jumbled order. This makes the
	# shitty closed-source black-box that is ifttt's RSS reader more likely
	# to actually publish our doubling graphs to social media
	# TODO: sorting this by the date so newer events occur first would be better
	keys = list( rssDoublings.keys() )
	random.shuffle( keys )

	# iterate through each country's doubling events in the past
	# <KEEP_ALL_PAGES_THIS_MANY_DAYS_BACK> days
	for countryCode in keys:

		# bail if there's nothing to generate for this region
		if rssDoublings[countryCode] == [[]]:
			continue

		thisDoubling = rssDoublings[countryCode][ len(rssDoublings[countryCode])-2 ]
		futureDoublings = rssDoublings[countryCode][ len(rssDoublings[countryCode])-1 ]

		# skip if we have no prediction on the next doubling
		if futureDoublings == list():
			print( "\tskipping " +countryCode+ " due to no doubling predictions" )
			continue

		# create a human-readable count number for the tweet
		thisCount = thisDoubling['count']
		if thisCount < 1000000:
			# if it's less than a million, just output the number
			thisCount = int(thisCount)
		elif thisCount >= 1000000 and thisCount < 1000000000:
			thisCount = str( int(thisCount/1000000) ) + " million"
		else:
			thisCount = str( int(thisCount/1000000000) ) + " billion"
		
		# create a human-readable count number for the tweet
		futureCount = futureDoublings[0]['count']
		if futureCount < 1000000:
			# if it's less than a million, just output the number
			futureCount = int(futureCount)
		elif futureCount >= 1000000 and futureCount < 1000000000:
			futureCount = str( int(futureCount/1000000) ) + " million"
		else:
			futureCount = str( int(futureCount/1000000000) ) + " billion"

		earliestPotential = int()
		lastPotential = int()
		for j,potential in enumerate(futureDoublings):

			# determine the potential doubling day that occurs first in time
			if potential['x'] < futureDoublings[earliestPotential]['x'] \
			 or earliestPotential == None:
				earliestPotential = j

			# determine the potential doubling day that occurs last in time
			if potential['x'] > futureDoublings[lastPotential]['x'] \
			 or lastPotential == None:
				lastPotential = j

		# the output of the range of days should only display a range if
		# the two dates aren't the same :)
		if futureDoublings[earliestPotential]['xlabel'] == futureDoublings[lastPotential]['xlabel']:
			dayRange = str(futureDoublings[earliestPotential]['xlabel'])
		else:
			dayRange = str(futureDoublings[earliestPotential]['xlabel'])+ " -<br/>" +str(futureDoublings[lastPotential]['xlabel'])

		# deltaRange
		if futureDoublings[earliestPotential]['delta'] == futureDoublings[lastPotential]['delta']:
			deltaRange = str(futureDoublings[earliestPotential]['delta'])
		else:
			deltaRange = str(futureDoublings[earliestPotential]['delta'])+ "-" +str(futureDoublings[lastPotential]['delta'])

		countryHashTag = country_to_code(countryCode).title()
		countryHashTag = ''.join( countryHashTag.split() )

		if countryCode == 'earth':
			link = "https://coviz.michaelaltfield.net/d1/" +str(x_to_date_label(thisDoubling['x']+1).strftime( "%Y-%m-%d") )
		else:
			link = "https://coviz.michaelaltfield.net/d1/" +str(x_to_date_label(thisDoubling['x']+1).strftime( "%Y-%m-%d") )+ "/" +countryCode

		#title = "#" +str(countryHashTag)+ " #COVID19 cases broke " +str(thisCount)+ ". Our models predict breaking " +str(futureCount)+ " will take " +str(deltaRange)+ " days #" +str(countryCode.upper())
		#title = "As #" +str(countryCode.upper())+ " #COVID19 cases breaks " +str(thisCount)+ ", our models predict breaking " +str(futureCount)+ " will take " +str(deltaRange)+ " days #" +countryHashTag
		title = "As #" +str(countryHashTag)+ " #COVID19 cases breaks " +str(thisCount)+ ", our models predict breaking " +str(futureCount)+ " will take " +str(deltaRange)+ " days #" +countryHashTag
		description = title+ "\n\n" +link
		#description += " \n\n#coronavirus #stayhome #dataviz #FlattenTheCurve #DataScience #socialdistance #coviz"
		pubDate = str( x_to_date_label( thisDoubling['x'] ).strftime( "%d %b %Y %H:%M:%S")+ " +0000" )

		content += '''
	<item>
		<title>''' +title+ '''</title>
		<link>''' +link+ '''</link>
		<guid>''' +link+ '''</guid>
		<description>''' +description+ '''</description>
	</item>
'''

	# bail if we're asked to update a file that doesn't exist
	if not os.path.isfile( rss_file_path ):
		print( "WARN: file doesn't exist (" +str(rss_file_path)+ ")" )
		return

	# first get the current file's contents
	with open( rss_file_path, 'r') as f:
		rss_file_contents = str(f.read())

	# now find the location of the boundaries for the contents we want to replace
	startStr = '</channel></rss>'
	startPos = rss_file_contents.find(startStr)

	# bail if we couldn't find the sting
	if startPos == -1:
		return

	oldContents = rss_file_contents[startPos:]
	newContents = content+ "\n</channel></rss>\n"

	# replace the old contents with the new one (we're essentially appending)
	rss_file_contents = rss_file_contents.replace( oldContents, newContents, 1 )

	# and write it out to the old index.html file
	with open( rss_file_path, 'w') as f:
		f.write( rss_file_contents )

def generate_rss( output_dir_path, relative_dir_path, chartday_cur ):

	print( "INFO: generating " +str(relative_dir_path)+ "/feed.xml" )

	rss_file_path = os.path.join( output_dir_path, relative_dir_path, 'feed.xml' )
	rss_build_date = datetime.utcnow().strftime("%d %b %Y %H:%M:%S")+ ' +0000'
	dateString = x_to_date_label( chartday_cur ).strftime( "%Y-%m-%d" )

	content = '''<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">

<channel>
	<atom:link href="https://coviz.michaelaltfield.net/e2b/feed.xml" rel="self" type="application/rss+xml" />
	<title>Coviz</title>
	<link>https://coviz.michaelaltfield.net/</link>
	<description>Coviz is an open-source project which aims to generate and publish visualizations from data on the spread of coronavirus (COVID-19)</description>
	<lastBuildDate>''' +rss_build_date+ '''</lastBuildDate>
	<language>en-US</language>'''

	# get a list of the regions we've output for this model and add them each
	# to our rss content
	regions =  os.listdir( os.path.join( output_dir_path, relative_dir_path, dateString ) )
	for region in regions:

		# skip a few types of files that we don't need to include in the RSS feed
		if region in ['index.html.gz', 'facebook.png', 'twitter.png']:
			continue

		if region == 'index.html':
			# this is actually our non-region-specific entry, which means "earth"

			title = "Today's global #COVID19 extrapolation chart now available"
			link = "https://coviz.michaelaltfield.net/" +relative_dir_path+ '/' +dateString+ "/"
			description = title+ "\n\n" +link
			#description += "\n\n#coronavirus #stayhome #dataviz #FlattenTheCurve #DataScience #socialdistance #coviz"

		else:
			# skip country-specific e2b daily chart updates for now to see if that
			# fixes rss -> ifttt -> social media issues
			continue

			title = "Today's #COVID19 extrapolation chart now available for #" +country_to_code(region).replace( " ", "")
			link = "https://coviz.michaelaltfield.net/" +relative_dir_path+ '/' +dateString+ "/" +region
			description = title+ "\n\n" +link+ "\n\n#coronavirus #stayhome #dataviz #FlattenTheCurve #DataScience #socialdistance #coviz"

		content += '''
	<item>
		<title>''' +title+ '''</title>
		<link>''' +link+ '''</link>
		<guid>''' +link+ '''</guid>
		<description>''' +description+ '''</description>
	</item>
'''
		
	content = content+ '\n</channel></rss>'

	# for the day's dir
	with open( rss_file_path, 'w') as f:
		f.write( content+ "\n" )

def update_old_data_link( html_file_path, href_latest, anchor_text ):

	# bail if we're asked to update a file that doesn't exist
	if not os.path.isfile( html_file_path ):
		print( "WARN: file doesn't exist (" +str(html_file_path)+ ")" )
		return

	# first get the current file's contents
	with open( html_file_path, 'r') as f:
		html_file_contents = str(f.read())

	# now find the location of the boundaries for the contents we want to replace
	startStr = '<!-- BOUNDARY_BEGIN=OLD_DATA_LINK -->'
	endStr = '<!-- BOUNDARY_END=OLD_DATA_LINK -->'
	startPos = html_file_contents.find(startStr)
	endPos = html_file_contents.find(endStr)

	# bail if we couldn't find the sting
	if startPos == -1 or endPos == -1:
		return

	oldContents = html_file_contents[startPos:endPos]

	newContents = '''
<!-- BOUNDARY_BEGIN=OLD_DATA_LINK -->
<div class="m-note m-danger">
<p>You are currently viewing a graph of old data. To see the most current graph, please visit:</p> <ul><li><a href="''' +href_latest+ '">' +str( anchor_text )+ '''</a></li></ul>
</div>
<!-- BOUNDARY_END=OLD_DATA_LINK -->
'''
	# replace the old contents (link) with the new one
	html_file_contents = html_file_contents.replace( oldContents, newContents, 1 )

	# and write it out to the old index.html file
	with open( html_file_path, 'w') as f:
		f.write( html_file_contents )

################################################################################
#                                   OBJECTS                                    #
################################################################################

class CovizModelsGenerator(object):

	def __init__(self, context, settings, path, theme, output_path, *null):

		self.path = path
		self.pages = []
		self.output_path = output_path
		self.context = context
		self.now = datetime.now()
		self.siteurl = settings.get('SITEURL')

		self.default_timezone = settings.get('TIMEZONE', 'UTC')
		self.timezone = getattr(self, 'timezone', self.default_timezone)
		self.timezone = timezone(self.timezone)

		config = settings.get('COVIZ_MODELS', {})

		if not isinstance(config, dict):
			warning("coviz_models plugin: the COVIZ_MODELS setting must be a dict")
		else:
			#fmt = config.get('format')
			todo = 'actually replace me with config sanity checks'

	# this is the function we use to add all our html-pages-to-be to the
	# "context". This "context" will later be made available to the
	# generate_output() function, which will decide what content to add to those
	# html pages
	#def generate_context( self ):
		# TODO: figure out how to use this

	def generate_output(self, writer):

		# template
		preContentHtml = ''
		postContentHtml = ''
		graphsInHtml = ''
		regionData = {}

		########
		# MAIN #
		########

		# this var will hold the sum of all the data (for the entire earth)
		earth = None

		# datestamp of now (when the graph is generated) that we'll overaly on the graph
		STAMP_GENERATED = datetime.utcnow()
		STAMP_GENERATED_STRING = str( STAMP_GENERATED.strftime("%Y-%m-%d %H:%M") + " UTC" )

		# URL to overlay on the graph as the "source"
		#sourceURL = 'https://coviz.org/e2a/' + str( datetime.now().strftime("%Y-%m-%d") )
		sourceURL = 'https://coviz.michaelaltfield.net/e2a'

		topLeftAnnotation =  'Source: ' +str(sourceURL)+ '<br />' \
		  +'Generated ' +str( STAMP_GENERATED_STRING )
		
		fig = go.Figure()

		# open the csv database
		with open( DATA_FILE_PATH, newline='') as dataFile:
			reader = csv.reader( dataFile )

			# loop through each country in the db
			for row in reader:

				data = row[4:]

				# if "earth" was specified, we need to sum up all the rows
				if "earth" in REGIONS:
					if row[2] == 'Lat':
						# don't count the header row
						earth = None
					elif earth == None:
						# this is the first row
						earth = data
					else:
						# for all subsequent rows, sum to our existing sums
						for i in range( len(earth) ):
							earth[i] = int(earth[i]) + int(data[i])

				# is this row for one of the countries that we want to process?
				if row[1] not in REGIONS:
					continue

				region = row[1]
				countryCode = country_to_code(region)

				# if this is just a province of a country, skip it
				if regionData.get(countryCode) == None:
					# this is the first row
					regionData[countryCode] = data
				else:
					# for all subsequent rows, sum to our existing sums
					for i in range( len(regionData[countryCode]) ):
						regionData[countryCode][i] = int(regionData[countryCode][i]) + int(data[i])

		# add the whole earth's sums to the regionData dict so it'll be
		# included in the loop below
		regionData['earth'] = earth

		for countryCode,data in regionData.items():

			print( ' ' )
			print(countryCode.upper())

			# produce this graph for today, but also produce it for previous days
			# start at day 76 = Apr 06. Note this means our first graph will be labled
			# April 06 but that its data will *end* on April 05.
			for lastDayNum in range( HISTORY_START, len(data)+1 ):

				# just take a subset of all our data, extending it by one day for this
				# iteration
				y = list( map(int, data[:lastDayNum]) )

				# A NOTE ABOUT WHAT A DAY FOR <THIS> GRAPH MEANS
				# the "chart" day is the day for which the chart is considered "latest". 
				# For example, someone visiting the site on April 15 would expect the most 
				# recent chart to have a title of "April 15". But that chart from April 15 
				# cant possibly have the data for April 15 as the day April 15 isnt done 
				# yet (it will only have the latest data from April 14). Therefore, we make
				# a distinction between the "chart" day and the "data" day. The "data" day 
				# should always be one day behind the "chart" day

				stamp_chart = x_to_date_label( len(y) )
				print( ' ' )
				sys.stdout.write( "\t" +str(stamp_chart.strftime( "%Y-%m-%d" )) )

				# skip this iteration if the page for this day is too old
				# or not a monthly day
				if not shouldWeHavePageForThisDay( lastDayNum, len(data) ):
					sys.stdout.write( " (skipped)" )
					continue

				# skip this iteration if we've already done it
				if os.path.isfile( 'public/e2b/' +stamp_chart.strftime("%Y-%m-%d")+ '/index.html') and lastDayNum < len(data):
					sys.stdout.write( " (skipped)" )

					# well, don't completely skip this day's graph; we need to update it to
					# have a hyperlink to our latest graph
					if countryCode == 'earth':
						update_old_data_link( self.output_path+ '/e2a/' +stamp_chart.strftime( "%Y-%m-%d")+ "/index.html", '/e2a/' +x_to_date_label( len(data) ).strftime("%Y-%m-%d")+ '/' +countryCode+ '/', x_to_date_label( len(data) ).strftime("%Y-%m-%d") )
						update_old_data_link( self.output_path+ '/e2b/' +stamp_chart.strftime( "%Y-%m-%d")+ "/index.html", '/e2b/' +x_to_date_label( len(data) ).strftime("%Y-%m-%d")+ '/', x_to_date_label( len(data) ).strftime("%Y-%m-%d") )
						update_old_data_link( self.output_path+ '/d1/' +stamp_chart.strftime( "%Y-%m-%d")+ "/index.html", '/d1/' +x_to_date_label( len(data) ).strftime("%Y-%m-%d")+ '/', x_to_date_label( len(data) ).strftime("%Y-%m-%d") )
					else:
						update_old_data_link( self.output_path+ '/e2a/' +stamp_chart.strftime( "%Y-%m-%d")+ '/' +countryCode+ "/index.html", '/e2a/' +x_to_date_label( len(data) ).strftime("%Y-%m-%d")+ '/' +countryCode+ '/', x_to_date_label( len(data) ).strftime("%Y-%m-%d") )
						update_old_data_link( self.output_path+ '/e2b/' +stamp_chart.strftime( "%Y-%m-%d")+ '/' +countryCode+ "/index.html", '/e2b/' +x_to_date_label( len(data) ).strftime("%Y-%m-%d")+ '/' +countryCode+ '/', x_to_date_label( len(data) ).strftime("%Y-%m-%d") )
						update_old_data_link( self.output_path+ '/d1/' +stamp_chart.strftime( "%Y-%m-%d")+ '/' +countryCode+ "/index.html", '/d1/' +x_to_date_label( len(data) ).strftime("%Y-%m-%d")+ '/' +countryCode+ '/', x_to_date_label( len(data) ).strftime("%Y-%m-%d") )

					continue

				# datestamp of the latest day in our dataset that we'll overaly on the graph
				stamp_dataset = x_to_date_label( len(y)-1 )

				baseGraph = generate_base_graph(
				 y,
				 lastDayNum,
				 "Hypothetical Data *<br />(after " +stamp_dataset.strftime("%b %d")+ ")",
				)

				#######
				# e2a #
				#######

				modelId = 'e2a'
				modelName = 'Second-Degree Polynomial Curve Fit #1'
				aboutSection = '''
<h2>About This Model</h2>

	<p>The chart above attempts to predict the future number of COVID-19 cases for the given region. The Y-axis shows the number of people who have tested positive and the X-axis is time. The vertical red line (labeled "Hypothetical Data") is the present day (on our latest graph). Everything to the right of the red line is the future, and its lines are calculated from the extrapolation model.</p>

	<p>In this model, there's three distinct predictions:</p>

	<ol class="bottompad">
	<li><strong>Last 30 days</strong> The purple line does a second-degree polynomial curve fit against the most-recent 30-days of data in the dataset</li>
	<li><strong>Last 7 days</strong> The green line does a curve fit on the most-recent 7 days of data
	<li><strong>Last 3 days</strong> The red line does a curve fit on the most-recent 3 days' data
	</ol>

	<p>You'll find that, depending on recent events in the past 3-days, the red line will flap around more wildly day-to-day than the others. This can be a useful coorelation indicator for the consequences of recent events, such as prematurely ending lockdowns.</p>

	<p>The "e2a" model shown here attempts to fit a curve using a second-degree polynomial using numpy's <code>poly1d()</code> function. This is Coviz's first model, and it is very simple. e2a is good for demonstrative purposes, yet it has many shortcomings.</p>

	<ul>
	<li>Model Short Name: e2a</li>
	<li>Model Full Name: Second-Degree Polynomial Curve Fit #1</li>
						<li>Submitted By: <a href="https://gitlab.com/mgoldenberg">Michael Goldenberg</a></li>
	</ul>

	<h3>Pros</h3>

	<ol>
	<li>Easy to write in python</li>
	<li>Easy to comprehend how it works</li>
	</ol>

	<h3>Cons</h3>
	<ol>
	<li>Expects exponential growth</li>
	<li>Doesn't take into account herd immunity</li>
	<li>Doesn't take into account history of previous pandemics</li>
	<li>Assumes y is infinite, yet there's a finite max human population</li>
	<li>May arch down parabolicly, yet the number of cases can never decrease</li>
	</ol>
	'''

				sourceURL = 'https://coviz.michaelaltfield.net/' + modelId
				title = "Projected Future Spread of COVID-19 (" +countryCode.upper()+ ", " \
				 +stamp_chart.strftime("%b %d")+ ')'

				# html-specific top-left
				topLeftAnnotation =  'Source: ' +str(sourceURL)+ '<br />' \
				 +'Generated ' +str( STAMP_GENERATED_STRING )

				# twitter-specific variables
				topLeftAnnotationTwitter =  '@coviz_org<br />' \
				 +'Generated ' +str( STAMP_GENERATED_STRING )
				bottomLeftAnnotationTwitter = "* " +sourceURL

				# facebook-specific variables
				topLeftAnnotationFacebook =  'fb.me/coviz.org<br />' \
				 +'Generated ' +str( STAMP_GENERATED_STRING )
				bottomLeftAnnotationFacebook = "* " +sourceURL

				# create a distinct graph from our base graph just for <this> extrapolation model
				graph_e2a = go.Figure( baseGraph )
				graph_e2a_twitter = go.Figure( baseGraph )
				graph_e2a_facebook = go.Figure( baseGraph )

				# draw the hypothetical plots on the graph generated above for the model "e2a"
				add_hypothetical_e2a( graph_e2a )
				graph_e2a = graph_add_title( graph_e2a, title )
				graph_e2a = graph_move_hypothetical_annotation( graph_e2a  )

				# copy the chart with the hypothetical projections into our twitter & facebook charts
				# but at this point they diverge (requiring different annotations, for example)
				graph_e2a_twitter = go.Figure( graph_e2a )
				graph_e2a_facebook = go.Figure( graph_e2a )

				# HTML-only
				graph_e2a = graph_add_top_left_annotation( graph_e2a, topLeftAnnotation )

				# WRITE OUT DAY-SPECIFIC CHARTS TO DISK
				if countryCode == 'earth':
					# this iteration, we're doing the whole earth (which has a different path)

					graph_write_html( graph_e2a, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d"), HISTORY_START, lastDayNum, len(data), modelId, modelName, aboutSection )

					graph_write_twitter_png( graph_e2a_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d"), topLeftAnnotationTwitter, bottomLeftAnnotationTwitter )

					graph_write_facebook_png( graph_e2a_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d"), topLeftAnnotationFacebook, bottomLeftAnnotationFacebook )

				else:
					# this is for a region that's not earth

					graph_write_html( graph_e2a, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d")+ '/' +countryCode, HISTORY_START, lastDayNum, len(data), modelId, modelName, aboutSection )

					graph_write_twitter_png( graph_e2a_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d")+ '/' +countryCode, topLeftAnnotationTwitter, bottomLeftAnnotationTwitter )

					graph_write_facebook_png( graph_e2a_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d")+ '/' +countryCode, topLeftAnnotationFacebook, bottomLeftAnnotationFacebook )

				# WRITE OUT MODEL-WIDE CHARTS TO DISK
				# and for the whole model's dir (only do this for the most-recent day)
				if countryCode == 'earth' and lastDayNum == len(data):
					graph_write_html( graph_e2a, self.output_path, modelId, HISTORY_START, lastDayNum, len(data), modelId, modelName, aboutSection )

				#######
				# e2b #
				#######

				modelId = 'e2b'
				modelName = 'Monotonic 2-Degree Polynomial Curve Fit (e2b)'
				aboutSection = '''
<h2>About This Model</h2>

	<p>The chart above attempts to predict the future number of COVID-19 cases for the given region. The Y-axis shows the number of people who have tested positive and the X-axis is time. The vertical red line (labeled "Hypothetical Data") is the present day (on our latest graph). Everything to the right of the red line is the future, and its lines are calculated from the extrapolation model.</p>

	<p>In this model, there's three distinct predictions:</p>

	<ol class="bottompad">
	<li><strong>Last 30 days</strong> The purple line does a second-degree polynomial curve fit against the most-recent 30-days of data in the dataset</li>
	<li><strong>Last 7 days</strong> The green line does a curve fit on the most-recent 7 days of data
	<li><strong>Last 3 days</strong> The red line does a curve fit on the most-recent 3 days' data
	</ol>

	<p>You'll find that, depending on recent events in the past 3-days, the red line will flap around more wildly day-to-day than the others. This can be a useful coorelation indicator for the consequences of recent events, such as prematurely ending lockdowns.</p>

	<p>The "e2b" model shown here attempts to fit a curve using a monotonic second-degree polynomial using numpy's <code>poly1d()</code> function. This is an improvement over Coviz's first model (<a href="/e2a/">e2a</a>) in that it prevents the fit line from decreasing. Still, this model is very simple and has many shortcomings.</p>

	<ul>
	<li>Model Short Name: e2b</li>
	<li>Model Full Name: Monotonic 2-Degree Polynomial Curve Fit</li>
						<li>Submitted By: <a href="https://gitlab.com/mgoldenberg">Michael Goldenberg</a></li>
	</ul>

	<h3>Pros</h3>

	<ol>
	<li>Easy to write in python</li>
	<li>Easy to comprehend how it works</li>
	</ol>

	<h3>Cons</h3>
	<ol>
	<li>Expects exponential growth</li>
	<li>Doesn't take into account herd immunity</li>
	<li>Doesn't take into account history of previous pandemics</li>
	<li>Assumes y is infinite, yet there's a finite max human population</li>
	<li>The monotonic implementation causes some nearly-negligable precision to be lost in the polynomial curve fit plot</li>
	</ol>
	'''

				sourceURL = 'https://coviz.michaelaltfield.net/' + modelId
				title = "Projected Future Spread of COVID-19 (" +countryCode.upper()+ ", " \
				 +stamp_chart.strftime("%b %d")+ ')'

				# html-specific top-left
				topLeftAnnotation =  'Source: ' +str(sourceURL)+ '<br />' \
				 +'Generated ' +str( STAMP_GENERATED_STRING )

				# twitter-specific variables
				topLeftAnnotationTwitter =  '@coviz_org<br />' \
				 +'Generated ' +str( STAMP_GENERATED_STRING )
				bottomLeftAnnotationTwitter = "* " +sourceURL

				# facebook-specific variables
				topLeftAnnotationFacebook =  'fb.me/coviz.org<br />' \
				 +'Generated ' +str( STAMP_GENERATED_STRING )
				bottomLeftAnnotationFacebook = "* " +sourceURL

				# create a distinct graph from our base graph just for <this> extrapolation model
				graph_e2b = go.Figure( baseGraph )
				graph_e2b_twitter = go.Figure( baseGraph )
				graph_e2b_facebook = go.Figure( baseGraph )

				# draw the hypothetical plots on the graph generated above for the model "e2b"
				add_hypothetical_e2b( graph_e2b )
				graph_e2b = graph_add_title( graph_e2b, title )
				graph_e2b = graph_move_hypothetical_annotation( graph_e2b  )

				# copy the chart with the hypothetical projections into our twitter & facebook charts
				# but at this point they diverge (requiring different annotations, for example)
				graph_e2b_twitter = go.Figure( graph_e2b )
				graph_e2b_facebook = go.Figure( graph_e2b )

				# HTML-only
				graph_e2b = graph_add_top_left_annotation( graph_e2b, topLeftAnnotation )

				if countryCode == 'earth':
					# this iteration, we're doing the whole earth (which has a different path)

					graph_write_html( graph_e2b, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d"), HISTORY_START, lastDayNum, len(data), modelId, modelName, aboutSection )

					graph_write_twitter_png( graph_e2b_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d"), topLeftAnnotationTwitter, bottomLeftAnnotationTwitter )

					graph_write_facebook_png( graph_e2b_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d"), topLeftAnnotationFacebook, bottomLeftAnnotationFacebook )

				else:
					# this is for a region that's not earth

					graph_write_html( graph_e2b, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d")+ '/' +countryCode, HISTORY_START, lastDayNum, len(data), modelId, modelName, aboutSection )

					graph_write_twitter_png( graph_e2b_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d")+ '/' +countryCode, topLeftAnnotationTwitter, bottomLeftAnnotationTwitter )

					graph_write_facebook_png( graph_e2b_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d")+ '/' +countryCode, topLeftAnnotationFacebook, bottomLeftAnnotationFacebook )

				# WRITE OUT MODEL-WIDE CHARTS TO DISK
				# and for the whole model's dir (only do this for the most-recent day)
				if countryCode == 'earth' and lastDayNum == len(data):
					graph_write_html( graph_e2b, self.output_path, modelId, HISTORY_START, lastDayNum, len(data), modelId, modelName, aboutSection )

					# finally, generate an rss file for feeding social media
					generate_rss( self.output_path, modelId, lastDayNum ) 

				#######
				# d1 #
				#######

				modelId = 'd1'
				modelName = 'Mono 2-Deg Poly Fit + Doubling (d1)'
				aboutSection = '''
<h2>About This Model</h2>

	<p>The chart above attempts to predict the future number of COVID-19 cases for the given region. The Y-axis shows the number of people who have tested positive and the X-axis is time. The vertical red line (labeled "Hypothetical Data") is the present day (on our latest graph). Everything to the right of the red line is the future, and its lines are calculated from the extrapolation model.</p>

	<p>In this model, there's three distinct predictions:</p>

	<ol class="bottompad">
	<li><strong>Last 30 days</strong> The purple line does a second-degree polynomial curve fit against the most-recent 30-days of data in the dataset</li>
	<li><strong>Last 7 days</strong> The green line does a curve fit on the most-recent 7 days of data
	<li><strong>Last 3 days</strong> The red line does a curve fit on the most-recent 3 days' data
	</ol>

	<p>You'll find that, depending on recent events in the past 3-days, the red line will flap around more wildly day-to-day than the others. This can be a useful coorelation indicator for the consequences of recent events, such as prematurely ending lockdowns.</p>

	<p>The "d1" model shown here builds on the <a href="/e2b/">e2b</a> model, which does a simple monotonic second-degree polynomial curve fit using numpy's <code>poly1d()</code> function.</p>

	<p>Unlike e2b, this model tracks the "doubling" time, which is the amount of time it takes for the number of COVID-19 cases in the given region to multiply by two. Doubling tracking begins at 1,000 cases. Therefore, the second doubling is 2,000. Then 4,000, 8,000, 16,000, etc</p>

	<p>At every doubling, a black tick is drawn on the graph indicating the height of the doubling in terms of number of cases. A vertical transparent "band" is also drawn behind each of these ticks. The distance between these bands over time gives a visual to the amount of time it takes for COVID-19 cases to double over time.</p>

	<p>To the right of the red "hypothetical data" line, predictions for the next date of doubling are marked with a dashed vertical black line originating from the x-axis and terminating at its cooresponding trace</p>

	<p>Below the chart is a data table that lists every doubling that occured in the historical dataset for the given region, starting with 1,000 cases. The last row in this table is a projection using the curve fitting algorithm to predict when the next doubling will occur in the future.</p>

	<p>This model is an improvement over Coviz's first model (<a href="/e2a/">e2a</a>), yet it's still very simple and has many shortcomings.</p>

	<ul>
	<li>Model Short Name: d1</li>
	<li>Model Full Name: Mono 2-Deg Poly Fit + Doubling</li>
						<li>Submitted By: <a href="https://www.michaelaltfield.net">Michael Altfield</a></li>
	</ul>

	<h3>Pros</h3>

	<ol>
	<li>Easy to write in python</li>
	<li>Easy to comprehend how it works</li>
	</ol>

	<h3>Cons</h3>
	<ol>
	<li>Expects exponential growth</li>
	<li>Doesn't take into account herd immunity</li>
	<li>Doesn't take into account history of previous pandemics</li>
	<li>Assumes y is infinite, yet there's a finite max human population</li>
	<li>The monotonic implementation causes some nearly-negligable precision to be lost in the polynomial curve fit plot</li>
	</ol>
	'''

				sourceURL = 'https://coviz.michaelaltfield.net/' + modelId
				title = "Projected Future Spread of COVID-19 (" +countryCode.upper()+ ", " \
				 +stamp_chart.strftime("%b %d")+ ')'

				# html-specific top-left
				topLeftAnnotation =  'Source: ' +str(sourceURL)+ '<br />' \
				 +'Generated ' +str( STAMP_GENERATED_STRING )

				# twitter-specific variables
				topLeftAnnotationTwitter =  '@coviz_org<br />' \
				 +'Generated ' +str( STAMP_GENERATED_STRING )
				bottomLeftAnnotationTwitter = "* " +sourceURL

				# facebook-specific variables
				topLeftAnnotationFacebook =  'fb.me/coviz.org<br />' \
				 +'Generated ' +str( STAMP_GENERATED_STRING )
				bottomLeftAnnotationFacebook = "* " +sourceURL

				# create a distinct graph from our base graph just for <this> extrapolation model
				graph_d1 = go.Figure( baseGraph )
				graph_d1_twitter = go.Figure( baseGraph )
				graph_d1_facebook = go.Figure( baseGraph )

				# draw the hypothetical plots on the graph generated above for the model "d1"
				doublings = add_hypothetical_d1( graph_d1, countryCode )
				graph_d1 = graph_add_title( graph_d1, title )
				graph_d1 = graph_move_hypothetical_annotation( graph_d1  )

				# add our d1 table below the graph but above the about section
				table = getDoublingTable_d1( doublings )
				aboutSection = '''
''' + table + '''
''' + aboutSection

				# copy the chart with the hypothetical projections into our twitter & facebook charts
				# but at this point they diverge (requiring different annotations, for example)
				graph_d1_twitter = go.Figure( graph_d1 )
				graph_d1_facebook = go.Figure( graph_d1 )

				# HTML-only
				graph_d1 = graph_add_top_left_annotation( graph_d1, topLeftAnnotation )

				if countryCode == 'earth':
					# this iteration, we're doing the whole earth (which has a different path)

					graph_write_html( graph_d1, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d"), HISTORY_START, lastDayNum, len(data), modelId, modelName, aboutSection )

					graph_write_twitter_png( graph_d1_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d"), topLeftAnnotationTwitter, bottomLeftAnnotationTwitter )

					graph_write_facebook_png( graph_d1_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d"), topLeftAnnotationFacebook, bottomLeftAnnotationFacebook )

				else:
					# this is for a region that's not earth

					graph_write_html( graph_d1, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d")+ '/' +countryCode, HISTORY_START, lastDayNum, len(data), modelId, modelName, aboutSection )

					graph_write_twitter_png( graph_d1_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d")+ '/' +countryCode, topLeftAnnotationTwitter, bottomLeftAnnotationTwitter )

					graph_write_facebook_png( graph_d1_twitter, self.output_path, modelId +'/'+ stamp_chart.strftime("%Y-%m-%d")+ '/' +countryCode, topLeftAnnotationFacebook, bottomLeftAnnotationFacebook )

				# WRITE OUT MODEL-WIDE CHARTS TO DISK
				# and for the whole model's dir (only do this for the most-recent day)
				if countryCode == 'earth' and lastDayNum == len(data):
					graph_write_html( graph_d1, self.output_path, modelId, HISTORY_START, lastDayNum, len(data), modelId, modelName, aboutSection )

					# generate an rss file of doubling events for feeding social media
					# I couldn't get ifttt to pickup the d1-specific rss file for the
					# life of me, so I ended-up just hijacking the e2b rss file
					generate_doubling_rss( self.output_path, 'e2b', lastDayNum ) 

		# print a final line delimiter
		print( ' ' )
		print( "INFO: Finished coviz.py generate_output()" )
