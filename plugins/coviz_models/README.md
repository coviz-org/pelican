TODO: documentation for this pelican plugin that I hope to be:

1. A pelican "generator" that will spit-out static-content html files with JS in them to display fancy graphs as well as png images of those graphs

1. These pages will be dynamically determined based on a query of an sqlite database; there will be no "writers" or "readers" for this plugin's content--the data source is the db

1. Designed in a robust way where this plugin will iterate through a directory with a set of scripts. each script will be a distinct extrapolation model that will produce a distinct page (and maybe history subpages). Ideally, these extrapolation model scrips can work in a context outside of pelican. Ideally, such that a user can write & test their own extrapolation models without even having pelican installed.
