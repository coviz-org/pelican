#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://coviz.michaelaltfield.net'
RELATIVE_URLS = False

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

# this was set to True from the repo we forked, but we need it to be False
# so we can maintain state between builds after downloading the artifacts from
# the last build (to decrease build time, not having to regenerate old graphs)
#DELETE_OUTPUT_DIRECTORY = True
DELETE_OUTPUT_DIRECTORY = False

# re-listing this just in-case I push this commented-out again in dev (for
# faster iteration times) and accidentally push it to origin again, breaking
# the website
PLUGINS = [ 'm.htmlsanity', 'm.qr', 'm.components', 'coviz_models' ]

# Following items are often useful when publishing

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""
