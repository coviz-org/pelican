Title: Support
save_as: contact/index.html

Support is available via tickets in gitlab

<ul>
<li><a href="https://gitlab.com/groups/coviz-org/-/issues">https://gitlab.com/groups/coviz-org/-/issues</a></li>
</ul>

<h1>Contact</h1>

You can write to The Coviz Project <a href="https://email.michaelaltfield.net">here</a>

<div class="m-row">

<div class="m-col-l-6">
<aside class="m-note m-info">

<p>If you're experiencing difficutly with CloudFlare's reCAPTCHA spam protection, you can get <a href="https://www.michaelaltfield.net">Michael Altfield's</a> email address (google-free) here:</p>

<ul>
<li><a href="https://email.michaelaltfield.net">https://email.michaelaltfield.net</a></li>
</ul>

</aside>
</div>

<div class="m-col-l-6">
<aside class="m-note m-info">

<p>Coviz is strictly an <a href="/about/">open-source</a> project. That said, if you'd like to exchange encrypted emails with Michael Altfield, you can do so using his <a href="https://email.michaelaltfield.net/pgp/pubkey.asc">public key</a> with the following fingerprint:</p>

<ul>
<li>PGP Fingerprint: <a href="https://keys.openpgp.org/vks/v1/by-fingerprint/0465E42F71206785E972644CFE1B84494E640D41">B162 9E1F 1737 EC4F 74C9  E923 1EF1 68D2 68C4 0535</a></li>
</li>

</aside>
</div>

</div>
