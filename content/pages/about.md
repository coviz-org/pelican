﻿Title: About

Coviz is an open-source project which aims to generate and publish visualizations from data on the spread of Coronavirus (COVID-19).

<div class="m-block m-badge m-success">

  <a href="https://www.michaelaltfield.net"><img src="/images/avatars/maltfield_square_100.jpg" alt="Photo" /></a>
  <h3>About the author</h3>
  <p>This website was built by <a href="https://www.michaelaltfield.net">Michael Altfield</a>. Please consider making a <a href="/donate/">donation</a> so Michael can keep this site running.</p>

	<div class="socialicons">

	<a href="https://twitter.com/MichaelAltfield">
		<img src="/images/social/twitter.png" />
	</a>
	<a href="https://mastodon.social/@MichaelAltfield">
		<img src="/images/social/mastodon.png" />
	</a>
	<a href="https://www.linkedin.com/in/michael-altfield-769a5b39">
		<img src="/images/social/linkedin.png" />
	</a>
	<a href="https://www.michaelaltfield.net/">
		<img src="/images/social/link.png" />
	</a>
	<a href="https://email.michaelaltfield.net/">
		<img src="/images/social/mail.png" />
	</a>

	</div>

</div>

<h2>Collaborators</h2>

I'd also like to thank the following folks who assisted me in putting together various aspects of this website.

<ul>
<li><a href="https://gitlab.com/mgoldenberg">Michael Goldenberg</a></li>
</ul>

<h2>Open Source</h2>

Coviz is an open-source project.

<div class="m-row">
<div class="m-col-m-6">
<aside class="m-note m-info">
<h3>Content</h3>
<p>
All content found on this website is copyleft under the CC-BY-SA license. Anyone is permitted to use this original or modified content in a personal or commercial project under the following conditions:
</p>

<ol class="bottompad">
	<li>Your project must display a prominent attribution for Coviz with a link back to this website</li>
	<li>Your project must also be copyleft under the CC-BY-SA or compatible license</li>
</ol>
</div>
</aside>

<div class="m-col-m-6">
<aside class="m-note m-info">
<h3>Code</h3>
<p>
All sourcecode used to generate this website is copyleft under the AGPLv3 license. Anyone is permitted to fork it from our <a href="https://gitlab.com/coviz-org/">gitlab.com repository</a> and use it in its original or modified form for a personal or commercial project under the following conditions:
</p>

<ol class="bottompad">
	<li>Your project must display a prominent attribution for Coviz with a link back to this website</li>
	<li>Your project must also be copyleft under the AGPLv3 or compatible license. You must make all of the source code of your project publicly available.</li>
</ol>

</aside>
</div>
</div>

For more information about the CC-BY-SA or AGPLv3 licenses, you can read them here:

<ul>
<li><a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International</a></li>
<li><a href="https://www.gnu.org/licenses/agpl-3.0.en.html">GNU Affero General Public License, Version 3</a></li>
</ul>

<h2>Data Source</h2>

The data used for producing the charts can be referenced in the '<code>data-*</code>' repositories of the <a href="https://gitlab.com/coviz-org">coviz-org group on gitlab.com</a>. This data originated from the following sources:

<ul>
<li>John Hopkins' Center for Systems Science and Engineering <a href="https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data">COVID-19 github.com</a> repository</li>
</ul>

<h2>Disclaimer</h2>

The models on this website take the historical data on the spread of the coronavirus and extrapolate them to attempt to predict the future. Of course, these models are guesswork.

The algorithms used in these models are open-source and contributed by the community. The community who submitted these algorithms may not be doctors, statisticians, data scientists, etc. The models may be wildly innaccurate and are provided without any warranty of any kind. For more information on the license under which this website's content is released, read our <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA license</a>.

<h2>Attribution</h2>

This website would not have been possible without the following tools and services: Python, Plotly, Docker, Nodejs, Gitlab, Cloudflare, Pelican, m.css, Jquery, Phplist, WebKit, Electron, Simple Icons, Gimp, InkScape, vim, QubesOS, and Debian GNU/Linux.

<h2>Useful Links</h2>

<div class="m-row">
<div class="m-col-m-6">

<aside class="m-note m-info">
<h3>Global Map of Cases (per capita)</h3>
<p>
<a href="https://ourworldindata.org/explorers/coronavirus-data-explorer?tab=map">Our World In Data</a> is my favorite website for getting a "real time" view of the status of the pandemic per-country around the world.

<br /><br />
<a href="https://ourworldindata.org/explorers/coronavirus-data-explorer?tab=map">
	<img src="/images/ourworldindata_202108.jpg" width="100%"/>
</a>

<br/><br/>
Each country is color-coded with a heatmap depending on the 7-day rolling average of the number of recorded cases per capita.
</p>
</aside>
</div>

<div class="m-col-m-6">
<aside class="m-note m-info">
<h3>Visual study of microdroplets</h3>
<p>
This <a href="https://www.weforum.org/agenda/2020/04/coronavirus-microdroplets-talking-breathing-spread-covid-19/">study in Japan</a> demonstrates how microdroplets or "<a href="https://www.wnycstudios.org/podcasts/radiolab/articles/dispatch-4-six-feet">mouth rain</a>" is spread by talking.

<br /><br />
<a href="https://www.weforum.org/agenda/2020/04/coronavirus-microdroplets-talking-breathing-spread-covid-19/">
	<img src="/images/microdroplets_202108.jpg" width="100%"/>
</a>

<br /><br />
This clearly demonstrates why distance, masks, <em>and</em> proper airflow is critical indoors.
</p>
</aside>
</div>

</div>
