Title: Extrapolation Models
:slug: models
:save_as: models/index.html

<p>This website provides a platform for people to <a href="/blog/covid-extrapolation/">develop and publish</a> their own extrapolation models from the COVID-19 dataset.</p>

<p>The models on this website take the historical data on the spread of the coronavirus and extrapolate them to attempt to predict the future. Of course, these models are guesswork and some are widely innacurate. We've done our best to outline the pros and cons of each model on their cooresponding model pages below</p>

<div class="m-row">
<div class="m-note m-push-t-1 m-default m-col-l-10">
<ol>
<li><a href="/e2a/">e2a</a> - Second-Degree Polynomial Curve Fit #1</li>
<li><a href="/e2b/">e2b</a> - Monotonic Second-Degree Polynomial Curve Fit</li>
<li><a href="/d1/">d1</a> - Mono 2-Deg Poly Fit + Doubling</li>
</ol>
</div>
</div>
<br style="clear:both;" />

As described in the links above, there are several shortcomings of such simple curve fitting algorithms. If you'd like to help us develop more Extrapolation Models to display on this website, please read our <a href="/blog/covid-extrapolation/">guide</a> to developing and submitting your own model.

