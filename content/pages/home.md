Title:
save_as: index.html
html_header: <meta http-equiv="refresh" content="0;url=/e2b/" />

The Coviz Project is an <a href="/about/">open-source</a> project that provides a platform for the community to extrapolate COVID-19 data to predict the spread of the coronavirus.

Please wait while you're redirected to the <a href="/e2b/">e2b model page</a>.

<!--
<iframe id="timeseries_confirmed_us" src="/coviz_models/graph.html" width="100%" height="500" style="display:none"></iframe>
<script type="text/javascript">document.getElementById('timeseries_confirmed_us').style.display = 'block';</script>
<noscript><img src="/coviz_models/graph.png" style="width:100%"/></noscript>
-->

