Donate
######

Thank you for supporting Coviz! Your donations will help cover the costs of running this website.

If you find this website a helpful resource to yourself or humanity, please consider making a donation.

Fiat
====

.. raw:: html

	<a href="https://ko-fi.com/covizorg"><img src="/images/ko-fi/BuyMeACoffee_blue-p-500.png" alt="Buy Me A Coffee" class="m-image" /></a>
	<br style="clear:both;" /><br />
	<a href="https://ko-fi.com/covizorg">Click here</a> to make a donation through Ko-fi.

Cryptocurrency
==============

If you'd rather make a donation without paying fees for unnecessary administrative overhead of Big Banks, you can send us cryptocurrency direct.

We accept bitcoin and monero.

.. raw:: html

	<div class="m-row">

	<div class="m-col-l-6" style="word-wrap:break-word;">
	<div class="m-note m-flat m-text-center">
		<h3>BTC Address</h3>

.. qr:: bitcoin:1DXyJpmu2KQMw2v4QJVzzjZo6f87BBndu6

1DXyJpmu2KQMw2v4QJVzzjZo6f87BBndu6

.. raw:: html

	</div>
	</div>

	<div class="m-col-l-6" style="word-wrap:break-word;">
	<div class="m-note m-flat m-text-center">
		<h3>XMR Address</h3>


.. qr:: monero:4B5ra5N1SN4d7BqDtkxAE5G5kGNz5mA5oCob41RzzoduM1uPAcr7QmNLzXtci5HvtkNXC7SowkxMjUUCXF2hm57MMS4jwkx 

4B5ra5N1SN4d7BqDtkxAE5G5kGNz5mA5oCob41RzzoduM1uPAcr7QmNLzXtci5HvtkNXC7SowkxMjUUCXF2hm57MMS4jwkx 

.. raw:: html

	</div>
	</div>

	</div>
