title: Introducing The Coviz Project
date: 2020-04-12
slug: introducing-coviz

The Coviz Project is an <a href="/about/">open-source</a> project that provides a platform for developing and publishing <a href="/models/">Extrapolation Models</a> to predict the future spread of coronavirus.

We're launching this site with only one very simple <a href="/e2a/">second-degree polynomial curve fitting (e2a)</a> model, but if you'd like to help contribute more sophisticated models that actually take into account things like herd immunity and data from past pandemics, please see our <a href="/blog/covid-extrapolation/">guide to developing and submitting</a> your own extrapolation models.

If you have any questions or comments, <a href="/contact/">click here</a> to get support or to contact us.

<br />
Thank you,<br/>
<a href="https://www.michaelaltfield.net">Michael Altfield</a>
