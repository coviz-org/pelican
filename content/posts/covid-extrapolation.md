title: Guide to Creating New Extrapolation Models
date: 2020-04-15
slug: covid-extrapolation

This article will describe how you can use our <a href=https://gitlab.com/coviz-org">open-source gitlab project</a> to develop your own <a href="/models/">extrapolation models</a> from the COVID-19 dataset.

We carefully chose our tools to make it fast & easy to get started developing your own models. It should take less than 10 minutes to:

<ol>
<li>clone our repo, </li>
<li>generate your first graph from our example models, and</li>
<li>begin writing your own models in python</li>
</ol>

<div class="m-note m-dim">
	This guide was written under the assumption that the user is using Debian 10 (buster). It may apply to other Debian-based systems (eg Ubuntu) as well, but has not been tested.
	<br /><br />
	If you encountered issues and managed to use our toolset on a non-Debian system, please open a <a href="https://gitlab.com/coviz-org/coviz-models/-/issues">ticket</a> on our gitlab and we'll add it to the wiki
</div>

<h2>Bootstrap</h2>

Coviz requires <code>python3</code> and <code>python3-pip</code>. It also requires <code>plotly</code> and <code>numpy</code> that will be installed from <code>pip3</code>.

Execute the following commands from a terminal to install these requirements

<pre class="m-code">
sudo apt-get -y install git python3-pip

# use --recurse-submodules so our <1M submodule repo with the CSV dataset is
# also fetched: https://gitlab.com/coviz-org/data-jhcsse
git clone --recurse-submodules git@gitlab.com:coviz-org/coviz-models.git

cd coviz-models
pip3 install requirements.txt
</pre>

<h2>Generate graphs</h2>

You should now have everything you need to generate your first graph.

Execute the following commands from the <code>coviz-models</code> directory to generate the graphs using our example extrapolation models.

<pre class="m-code">
./generateGraphs.py
ls
</pre>

The above <code>./generateGraphs.py</code> should have created a new directory called <code>output</code>. Inside the <code>output</code> directory you'll see three more directories--one for each of the different extrapolation models:

<pre class="m-code">
user@coviz:~/sandbox/coviz-models$ ls -1 output/
my-new-model
projections-based-on-last-three-days
projections-based-on-last-seven-days
user@coviz:~/sandbox/coviz-models$ 
</pre>

Inside of each of those models' directories, you'll an html file for each of the dataset's regions (ie: Afghanistan, US, Diamond Princess, etc).

<pre class="m-code">
user@coviz:~/sandbox/coviz-models$ ls output/projections-based-on-last-three-days/
us.html
user@coviz:~/sandbox/coviz-models$ 
</pre>

By default (for faster execution), only the US graphs are created. If you'd like to generate graphs for additional regions, specify one or more regions with <code>--region</code>. If you'd like to generate the graphs for all the regions, use <code>--earth</code>.

<div class="m-note m-dim">
Note: Using <code>--earth</code> will take a long time!
</div>

You can also specify <code>--help</code> for a list and description of all the options to <code>generateGraphs.py</code>.

<pre class="m-code">
./generateGraphs.py --help
</pre>

<h2>Viewing Graphs</h2>

<p>To view the graphs generated above from <code>./generateGraphs.py</code>, open the html file <code>output/&lt;model&gt;/&lt;region&gt;.html</code> (eg <code>output/projections-based-on-last-three-days/us.html</code>) in your browser to see the graphs.</p>

<pre class="m-code">
firefox output/projections-based-on-last-three-days/us.html
</pre>

<h2>Creating your own model</h2>

<p>If you'd like to create your own extraplation model, you can edit the <code>models/my_new_model.py</code> file to your liking.</p>

<p>You just need its <code>make_extrapolate</code> function to return a function that will take a single argument (the x value on the graph) and return another number (that x's cooresponding y value on the graph).</p>

<p>By default, the <code>models/my_new_model.py</code> script is just a copy of the <code>e2a_seven</code> extrapolation model, which does a simple curve fit against the most recent seven days of data using a second-degree polynomial with numpy's <code>poly1d()</code> function.</p>

<p>Let's change the <code>models/my_new_model.py</code> to do a curve fit against the most recent thirty days instead of seven.</p>

<p>Execute the following command to edit the file <code>models/my_new_model.py</code> in <code>gedit</code>.</p>

<pre class="m-code">
gedit models/my_new_model.py
</pre>

<p>The first thing you'll notice is how short the script is! Python's <code>numpy</code> module is fantastic, and it does most of the heavy lifting. Here's the entire file.</p>

<pre class="m-code">
import numpy

def make_extrapolate(data):
	x = [i for i in range(len(data))]
	y = data

	# fit exponential curve to last seven days of data 
	curve = numpy.polyfit(x[-7:], y[-7:], 2)

	# create function to be applied for extrapolation
	extrapolate = numpy.poly1d(curve)

	return extrapolate


def meta():
	return {
		'title': 'My New Model'
	}
</pre>

The important line that does the curve-fit is this one:

<pre class="m-code">
curve = numpy.polyfit(x[-7:], y[-7:], 2)
</pre>

<p>You can find the python documentation on numpy's <code>polyfit()</code> function <a href="https://numpy.org/doc/1.18/reference/generated/numpy.polyfit.html#numpy.polyfit">here</a>.</p>

<ol class="bottompad">
<li>The first argument to the <code>polyfit()</code> function is <code>x</code>, which is a list of x coordinates</li>
<li>The second argument to the <code>polyfit()</code> function is <code>y</code>, which is a list of y coordinates</li>
<li>The third argument to the <code>polyfit()</code> function is <code>deg</code>, which is an <code>int</code> that defines the degree of the fitting polynonomial. In all our example models, we use a second-degree fit.
</ol>

<p>Go ahead and change this line to the following, which will increase the set of data passed to the <code>polyfit()</code> function from the most-recent 7 days of the data set to the most-recent 30 days of the dataset.</p>

<pre class="m-code">
curve = numpy.polyfit(x[-30:], y[-30:], 2)
</pre>

<p>Save an close the file <code>my_new_model.py</code>, and re-generate the graphs.</p>

<pre class="m-code">
./generateGraphs.py
</pre>

<p>Now open the <code>output/my-new-model/us.html</code> file in your browser.</p>

<pre class="m-code">
firefox output/my-new-model/us.html
</pre>

<p>Your browser will now show you the second-degree polynomial curve fit changed to fit against the most-recent 30 days.</p>

<p>You can confirm this by looking at the difference between the output of the other two models</p>

<pre class="m-code">
firefox output/projections-based-on-last-three-days/us.html
firefox output/projections-based-on-last-seven-days/us.html
</pre>

<h2>make_extrapolate()</h2>

<p>Now you can make whatever modifications you'd like to the <code>my_new_model.py</code> file's <code>make_extrapolate()</code> function and follow the above procedure to generate its graph and check the result in your web browser.</p>

<p>In fact, you're not constrained to using <code>numpy</code>. The only constraint is that your <code>make_extrapolate()</code> function should return a function that takes <code>x</code> values and returns <code>y</code> values. Simple, right?</p>

<h2>Submitting Extrapolation Models</h2>

<p>Did you build an awesome extrapolation model from this guide and want to share it with the world? Great!</p>

<p>You can submit a ticket on our gitlab group for this. Make sure to include:</p>

<ol class="bottompad">
<li>The python code to produce the model (ie: the contents of <code>my_new_model.py</code>)</li>
<li>A human-readable name for the model (&lt; 45 characters)</li>
<li>A short id for the model (3-5 characters)</li>
<li>A short description of the model (~1-10 sentences)</li>
<li>A list of the pros of the model</li>
<li>A list of the cons of the model</li>
<li>A statement that by submitting their model to us, you are releasing the model copyleft under the CC-BY-SA license and its implementation code AGPLv3. You will need to state that [a] you are the original author and [b] that you permit anyone to use the model and any content produced by it under the terms of those licenses</li>
<li>A list of the names of the authors & contributors for developing the model (optional)</li>
<li>For each author/contributor, a hyperlink to a URL of their choice, such as a website or social media account for attribution (optional)</li>
</ol>

<p>Please <a href="https://gitlab.com/coviz-org/coviz-models/-/issues">create a new ticket on our github</a> with the above information, and we'll work on integrating the model into our website. Thank you!</p>

<h2>Updating the Dataset</h2>

<p>As the days pass, your data will become stale. The dataset should be updated once-a-day.</p>

<p>Execute the following command from the <code>coviz-models</code> directory to update the repo and its dataset submodule.</p>

<pre class="m-code">
git submodule foreach git pull origin master
git pull
</pre>

<h1>See Also</h1>

<ol>
<li><a href="https://gitlab.com/coviz-org/coviz-models">Our coviz-models git repo on gitlab</a></li>
<li><a href="/models/">A list of our current models</a></li>
<li><a href="/e2a/">Our "e2a" model</a></li>
</ol>

<h1>Further Reading</h1>

<ol>
<li><a href="https://en.wikipedia.org/wiki/Curve_fitting">Curve Fitting (Wikipedia)</a></li>
<li><a href="https://en.wikipedia.org/wiki/Spanish_flu">1918 Spanish Flu Pandemic (Wikipedia)</a></li>
<li><a href="https://en.wikipedia.org/wiki/Herd_immunity">Herd Immunity (Wikipedia)</a></li>
<li><a href="https://numpy.org/doc/1.18/user/index.html">numpy's User Guide</a></li>
<li><a href="https://numpy.org/doc/1.18/reference/routines.polynomials.html">numpy's "polynomials" documentation</a></li>
<li><a href="https://numpy.org/doc/1.18/reference/routines.polynomials.poly1d.html#fitting">numpy's "fitting polynomials" documentation</a></li>
</ol>
