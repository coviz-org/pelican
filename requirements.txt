pelican ~= 4.1.0
markdown
typogrify
plotly == 4.6.0
numpy == 1.18.2
psutil
requests
qrcode
