#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Michael Altfield'
#SITENAME = 'Coviz: visualizing the spread of Coronavirus (COVID-19)'
SITENAME = 'Coviz'
SITEURL = ''
DEFAULT_DATE = 'fs'

# move the blog posts to a subdir
PATH = 'content'
ARTICLE_PATHS = [ 'posts' ]
ARTICLE_SAVE_AS = 'blog/{slug}/index.html'
ARTICLE_URL = 'blog/{slug}/index.html'
INDEX_SAVE_AS = 'blog/index.html'
INDEX_URL = 'blog/'

# and put the pages in the root
PAGE_PATHS = [ 'pages' ]
PAGE_SAVE_AS = '{slug}/index.html'
PAGE_URL = '{slug}/index.html'

# skip translations, fixes error:
#   CRITICAL: IndexError: tuple index out of range
PAGE_TRANSLATION_ID = None

OUTPUT_PATH = 'public'
STATIC_PATHS = [ 'images', 'deps', 'data' ]
EXTRA_PATH_METADATA = {
 'images/covizlogo_20200410_7/favicon.ico': {'path': 'favicon.ico'},
 'images/covizlogo_20200410_7/apple-touch-icon.png': {'path': 'apple-touch-icon.png'},
}

TIMEZONE = 'UTC'
DEFAULT_LANG = 'en'

THEME = 'm.css/pelican-theme'
THEME_STATIC_DIR = 'static'
DIRECT_TEMPLATES = ['index']
M_CSS_FILES = [
	 '/static/m-light.css',
    '/deps/css/coviz.css'
]
#M_THEME_COLOR = '#22272e'
M_THEME_COLOR = '#cb4b16'
M_HTML_HEADER = """
<!-- Global site tag (gtag.js) - Google Analytics -->
<script defer src="https://www.googletagmanager.com/gtag/js?id=UA-163354069-1"></script>
<script defer src="/deps/js/ga/gtag.js"></script>
"""
M_SITE_LOGO = "/images/covizlogo_20200410_7/coviz-rectangle_200.png"
M_FAVICON = ('favicon.ico', 'image/x-ico')
M_HIDE_ARTICLE_SUMMARY = True
M_SOCIAL_IMAGE = '/images/featured/e2a_2020-04-07_twitter.jpg'

# we modified the theme to put this in an alt attribute on the image instead of
# displaying it next to the logo
M_SITE_LOGO_TEXT = ' '
M_SITE_LOGO_ALT = "coviz"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
M_LINKS_NAVBAR1 = [
 ('About', 'about/', 'about', []),
 ('Models', 'models/', 'models', [
  ('e2a', 'e2a/', ''),
  ('e2b', 'e2b/', ''),
  ('d1', 'd1/', ''),
 ]),
]
M_LINKS_NAVBAR2 = [
 ('Blog', 'blog/', '[blog]', []),
 ('Donate', 'donate/', 'donate', []),
]

M_LINKS_FOOTER1 = [
 ('Coviz', '/'),
 ('About', '/about/'),
 ('Donate', '/donate/'),
 ('Blog', '/blog/'),
]

M_LINKS_FOOTER2 = [
 ('Legal', ''),
 ('Privacy Policy', '/privacy-policy/'),
 ('Contact', '/contact/'),
]

M_LINKS_FOOTER3 = [
 ('Models', 'models/'),
 ('e2a', 'e2a/'),
 ('e2b', 'e2b/'),
 ('d1', 'd1/'),
]

M_LINKS_FOOTER4 = [
 ('Social', ''),
 ('Twitter', 'https://twitter.com/coviz_org'),
 ('Facebook', 'https://www.facebook.com/coviz.org'),
 ('Mastodon', 'https://mastodon.social/@coviz_org'),
 ('Gitlab', 'https://gitlab.com/coviz-org/'),
]

M_FINE_PRINT = """
.. raw:: html

	<div style="display: flex; align-items: center; justify-content:center; flex-wrap:wrap;">

	<a href="/donate/">
		<img class="m-image" src="/images/ko-fi/BuyMeACoffee_blue-p-500.png" alt="Buy Me A Coffee" width="200" style="margin-bottom:0" />
	</a>

	</div>

	<br style="clear:both;" />

Copyright © 2020 `Michael Altfield <https://www.michaelaltfield.net>`_. All rights reserved. Content licensed `CC-BY-SA <http://creativecommons.org/licenses/by-sa/4.0/>`_. Powered by `Pelican <https://getpelican.com>`_ and `m.css <https://mcss.mosra.cz>`_.
"""

PLUGIN_PATHS = [ 'plugins', 'm.css/plugins' ]
#PLUGINS = [ 'm.htmlsanity', 'm.qr', 'm.components', 'm.code' ]
PLUGINS = [ 'm.htmlsanity', 'm.qr', 'm.components', 'm.code', 'coviz_models' ]

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
