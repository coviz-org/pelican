FROM node:13-buster-slim

# update and install should be on the same RUN statement to prevent
# "Using cache" issues in docker
#  * https://stackoverflow.com/questions/27273412/cannot-install-packages-inside-docker-ubuntu-image
RUN apt-get update && apt-get install -y python3 python3-pip xvfb libxcomposite1 libxcursor1 libxi6 libxtst6 libnss3 libgdk-pixbuf2.0 libgtk-3-0 libxss1 libasound2 curl unzip git

COPY requirements.txt .
RUN pip3 install -r requirements.txt

RUN npm install -g --unsafe-perm=true electron orca
